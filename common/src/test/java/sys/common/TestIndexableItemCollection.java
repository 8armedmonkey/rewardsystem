package sys.common;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TestIndexableItemCollection {

    private SampleIndexableItemCollection c;

    @Before
    public void setUp() {
        c = new SampleIndexableItemCollection();
    }

    @Test(expected = DuplicateItemException.class)
    public void add_itemWithSameIdExists_throwException() {
        c.add(new SampleIndexableItem("id", "item"));
        c.add(new SampleIndexableItem("id", "item"));
    }

    @Test
    public void add_noItemWithSameId_itemAdded() {
        c.add(new SampleIndexableItem("id-1", "item-1"));
        c.add(new SampleIndexableItem("id-2", "item-2"));

        assertEquals("id-1", c.get(0).getIndex());
        assertEquals("id-2", c.get(1).getIndex());
    }

    @Test
    public void remove_item_itemRemoved() {
        SampleIndexableItem item = new SampleIndexableItem("id-1", "item-1");

        c.add(item);
        c.remove(item);

        assertFalse(c.contains(item));
    }

    @Test
    public void remove_existentPosition_itemRemoved() {
        SampleIndexableItem item = new SampleIndexableItem("id-1", "item-1");

        c.add(item);
        c.remove(0);

        assertFalse(c.contains(item));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void remove_nonExistentPosition_exceptionThrown() {
        c.add(new SampleIndexableItem("id-1", "item-1"));
        c.remove(1);
    }

    @Test
    public void clear_allItemsRemoved() {
        c.add(new SampleIndexableItem("id-1", "item-1"));
        c.add(new SampleIndexableItem("id-2", "item-2"));
        c.clear();

        assertEquals(0, c.size());
        assertFalse(c.containsKey("id-1"));
        assertFalse(c.containsKey("id-2"));
    }

    @Test
    public void replace_itemWithSameIdExists_itemReplaced() {
        c.replace(new SampleIndexableItem("id", "item-old-label"));
        c.replace(new SampleIndexableItem("id", "item-new-label"));

        assertEquals(1, c.size());
        assertEquals("id", c.get(0).getIndex());
        assertEquals("item-new-label", c.get(0).getLabel());
    }

    @Test
    public void replace_noItemWithSameId_itemAdded() {
        c.replace(new SampleIndexableItem("id-1", "item-1"));
        c.replace(new SampleIndexableItem("id-2", "item-2"));

        assertEquals(2, c.size());
        assertEquals("id-1", c.get(0).getIndex());
        assertEquals("item-1", c.get(0).getLabel());
        assertEquals("id-2", c.get(1).getIndex());
        assertEquals("item-2", c.get(1).getLabel());
    }

    @Test
    public void getByIndex_itemExists_itemReturned() {
        c.add(new SampleIndexableItem("id-1", "item-1"));
        c.add(new SampleIndexableItem("id-2", "item-2"));

        SampleIndexableItem i1 = c.getByIndex("id-1");
        SampleIndexableItem i2 = c.getByIndex("id-2");

        assertEquals("id-1", i1.getIndex());
        assertEquals("item-1", i1.getLabel());
        assertEquals("id-2", i2.getIndex());
        assertEquals("item-2", i2.getLabel());
    }

    @Test
    public void getByIndex_itemDoesNotExist_nullReturned() {
        c.add(new SampleIndexableItem("id-1", "item-1"));
        c.add(new SampleIndexableItem("id-2", "item-2"));

        assertNull(c.getByIndex("id-3"));
    }

    @Test
    public void containsKey_indexAndItemExists_trueReturned() {
        c.add(new SampleIndexableItem("id-1", "item-1"));
        c.add(new SampleIndexableItem("id-2", "item-2"));

        assertTrue(c.containsKey("id-1"));
    }

    @Test
    public void containsKey_indexAndItemDoesNotExist_falseReturned() {
        c.add(new SampleIndexableItem("id-1", "item-1"));
        c.add(new SampleIndexableItem("id-2", "item-2"));

        assertFalse(c.containsKey("id-3"));
    }

}
