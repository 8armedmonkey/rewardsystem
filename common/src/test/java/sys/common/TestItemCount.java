package sys.common;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestItemCount {

    private ItemCount<String> itemCount;

    @Before
    public void setUp() {
        itemCount = new ItemCount<>();
    }

    @Test
    public void set_keyCount_keyCountSet() throws Exception {
        itemCount.set("index", 1);
        assertEquals(1, itemCount.get("index"));
    }

    @Test(expected = KeyNotExistsException.class)
    public void get_keyNotExists_exceptionThrown() {
        itemCount.set("index1", 1);
        itemCount.get("index2");
    }

    @Test
    public void increment_keyExists_countIncremented() {
        itemCount.set("index", 2);
        assertEquals(3, itemCount.increment("index"));
        assertEquals(3, itemCount.get("index"));
    }

    @Test(expected = KeyNotExistsException.class)
    public void increment_keyNotExist_countRemains() {
        itemCount.set("index1", 2);
        itemCount.increment("index2");
        assertEquals(2, itemCount.get("index1"));
    }

    @Test
    public void decrement_keyExists_countDecremented() throws Exception {
        itemCount.set("index", 2);
        assertEquals(1, itemCount.decrement("index"));
        assertEquals(1, itemCount.get("index"));
    }

    @Test(expected = KeyNotExistsException.class)
    public void decrement_keyNotExists_countRemains() throws Exception {
        itemCount.set("index1", 2);
        itemCount.decrement("index2");
        assertEquals(2, itemCount.get("index1"));
    }

    @Test
    public void size_sizeReturned() {
        itemCount.set("index1", 0);
        itemCount.set("index2", 0);
        itemCount.set("index3", 0);
        assertEquals(3, itemCount.size());
    }

}
