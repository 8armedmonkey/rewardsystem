package sys.common;

import java.io.Serializable;

public class SampleItem implements Serializable {

    private String label;

    public SampleItem(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

}
