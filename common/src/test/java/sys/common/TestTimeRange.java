package sys.common;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestTimeRange {

    @Test(expected = IllegalArgumentException.class)
    public void constructor_minGtMax_throwIllegalArgumentException() {
        new TimeRange(3000L, 1000L);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_minEqualMax_throwIllegalArgumentException() {
        new TimeRange(1000L, 1000L);
    }

    @Test
    public void contains_valueIsWithinRange_returnTrue() {
        TimeRange t = new TimeRange(1000L, 3000L);

        assertTrue(t.contains(1000L));
        assertTrue(t.contains(2000L));
        assertTrue(t.contains(3000L));
    }

    @Test
    public void contains_valueIsOutOfRange_returnFalse() {
        TimeRange t = new TimeRange(1000L, 3000L);

        assertFalse(t.contains(0L));
        assertFalse(t.contains(5000L));
    }

}
