package sys.common;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestIntRange {

    private IntRange range;

    @Before
    public void setUp() {
        range = new IntRange(0, 2);
    }

    @Test
    public void contains_valueEqualsToMin_returnTrue() {
        assertTrue(range.contains(0));
    }

    @Test
    public void contains_valueInBetweenMinAndMax_returnTrue() {
        assertTrue(range.contains(1));
    }

    @Test
    public void contains_valueEqualsToMax_returnTrue() {
        assertTrue(range.contains(2));
    }

    @Test
    public void contains_valueLessThanMin_returnFalse() {
        assertFalse(range.contains(-1));
    }

    @Test
    public void contains_valueGreaterThanMax_returnFalse() {
        assertFalse(range.contains(3));
    }

}
