package sys.common;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestItemCollection {

    private SampleItemCollection c;

    @Before
    public void setUp() {
        c = new SampleItemCollection();
    }

    @Test
    public void constructor_nonNullItems_itemsAdded() {
        c = new SampleItemCollection(new SampleItem("item-1"), new SampleItem("item-2"));
        assertEquals(2, c.size());
    }

    @Test
    public void constructor_nullItems_noItemsAdded() {
        c = new SampleItemCollection(null);
        assertEquals(0, c.size());
    }

    @Test
    public void add_item_itemAdded() {
        c.add(new SampleItem("item-1"));
        c.add(new SampleItem("item-2"));

        assertEquals(2, c.size());
    }

    @Test
    public void addIfNotExists_itemNotExists_itemAdded() {
        c.addIfNotExists(new SampleItem("item-1"));

        assertEquals(1, c.size());
        assertEquals("item-1", c.get(0).getLabel());
    }

    @Test
    public void addIfNotExists_itemAlreadyExists_itemAdded() {
        SampleItem i = new SampleItem("item-1");

        c.addIfNotExists(i);
        c.addIfNotExists(i);
        c.addIfNotExists(i);

        assertEquals(1, c.size());
        assertEquals("item-1", c.get(0).getLabel());
    }

    @Test
    public void remove_item_itemRemoved() {
        SampleItem i1 = new SampleItem("item-1");
        SampleItem i2 = new SampleItem("item-2");

        c.add(i1);
        c.add(i2);
        c.remove(i1);

        assertEquals(1, c.size());
        assertEquals("item-2", c.get(0).getLabel());
    }

    @Test
    public void remove_position_itemRemoved() {
        c.add(new SampleItem("item-1"));
        c.add(new SampleItem("item-2"));
        c.remove(0);

        assertEquals(1, c.size());
        assertEquals("item-2", c.get(0).getLabel());
    }

    @Test
    public void get_position_itemReturned() {
        c.add(new SampleItem("item-1"));
        c.add(new SampleItem("item-2"));

        assertEquals("item-1", c.get(0).getLabel());
        assertEquals("item-2", c.get(1).getLabel());
    }

    @Test
    public void clear_allItemsRemoved() {
        c.add(new SampleItem("item-1"));
        c.add(new SampleItem("item-2"));
        c.clear();

        assertEquals(0, c.size());
    }

}
