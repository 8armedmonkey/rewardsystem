package sys.common;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TestItemCountUp {

    private ItemCountUp<String> itemCountUp;

    @Before
    public void setUp() {
        itemCountUp = new ItemCountUp<>();
    }

    @Test
    public void set_keyCount_keyCountSet() throws Exception {
        itemCountUp.set("index", 0, 1);
        assertEquals(0, itemCountUp.getCurrent("index"));
        assertEquals(1, itemCountUp.getExpected("index"));
    }

    @Test(expected = KeyNotExistsException.class)
    public void getCurrent_keyNotExists_exceptionThrown() {
        itemCountUp.set("index1", 0, 1);
        itemCountUp.getCurrent("index2");
    }

    @Test(expected = KeyNotExistsException.class)
    public void getExpected_keyNotExists_exceptionThrown() {
        itemCountUp.set("index1", 0, 1);
        itemCountUp.getExpected("index2");
    }

    @Test
    public void increment_keyExists_countIncremented() {
        itemCountUp.set("index", 0, 1);
        assertEquals(1, itemCountUp.increment("index"));
        assertEquals(1, itemCountUp.getCurrent("index"));
    }

    @Test(expected = KeyNotExistsException.class)
    public void increment_keyNotExist_countRemains() {
        itemCountUp.set("index1", 0, 1);
        itemCountUp.increment("index2");
        assertEquals(1, itemCountUp.getCurrent("index1"));
    }

    @Test
    public void size_sizeReturned() {
        itemCountUp.set("index1", 0, 0);
        itemCountUp.set("index2", 1, 1);
        itemCountUp.set("index3", 2, 2);
        assertEquals(3, itemCountUp.size());
    }

    @Test
    public void hasReachedExpectedCounts_allItemsReachedExpectedCounts_returnTrue() {
        itemCountUp.set("index1", 1, 1);
        itemCountUp.set("index2", 2, 2);
        itemCountUp.set("index3", 3, 3);
        assertTrue(itemCountUp.hasReachedExpectedCounts());
    }

    @Test
    public void hasReachedExpectedCounts_someItemsReachedExpectedCounts_returnFalse() {
        itemCountUp.set("index1", 0, 1);
        itemCountUp.set("index2", 2, 2);
        itemCountUp.set("index3", 3, 3);
        assertFalse(itemCountUp.hasReachedExpectedCounts());
    }

}
