package sys.common;

public class SampleItemCollection extends ItemCollection<SampleItem> {

    public SampleItemCollection() {
    }

    public SampleItemCollection(SampleItem... sampleItems) {
        super(sampleItems);
    }

}
