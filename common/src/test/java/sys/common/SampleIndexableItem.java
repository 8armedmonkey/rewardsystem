package sys.common;

import java.io.Serializable;

public class SampleIndexableItem implements Indexable<String>, Serializable {

    private String id;
    private String label;

    public SampleIndexableItem(String id, String label) {
        this.id = id;
        this.label = label;
    }

    @Override
    public String getIndex() {
        return id;
    }

    public String getLabel() {
        return label;
    }

}
