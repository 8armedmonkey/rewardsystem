package sys.common;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static java.lang.System.identityHashCode;
import static org.junit.Assert.assertEquals;
import static sys.common.SortOrder.ASC;
import static sys.common.SortOrder.DESC;

public class TestBooleanComparator {

    private BooleanComparator c;

    @Before
    public void setUp() {
        c = new BooleanComparator();
    }

    @Test
    public void compare___true_true_ASC___returnZero() {
        c.setSortOrder(ASC);
        assertEquals(0, c.compare(true, true));
    }

    @Test
    public void compare___false_false_ASC___returnZero() {
        c.setSortOrder(ASC);
        assertEquals(0, c.compare(false, false));
    }

    @Test
    public void compare___true_false_ASC___returnPosOne() {
        c.setSortOrder(ASC);
        assertEquals(1, c.compare(true, false));
    }

    @Test
    public void compare___false_true_ASC___returnNegOne() {
        c.setSortOrder(ASC);
        assertEquals(-1, c.compare(false, true));
    }

    @Test
    public void compare___true_true_DESC___returnZero() {
        c.setSortOrder(DESC);
        assertEquals(0, c.compare(true, true));
    }

    @Test
    public void compare___false_false_DESC___returnZero() {
        c.setSortOrder(DESC);
        assertEquals(0, c.compare(false, false));
    }

    @Test
    public void compare___true_false_DESC___returnNegOne() {
        c.setSortOrder(DESC);
        assertEquals(-1, c.compare(true, false));
    }

    @Test
    public void compare___false_true_DESC___returnPosOne() {
        c.setSortOrder(DESC);
        assertEquals(1, c.compare(false, true));
    }

    @Test
    public void sort___allTrue_ASC___noChanges() {
        c.setSortOrder(ASC);

        SampleBooleanItem o1, o2, o3, o4, o5;

        List<SampleBooleanItem> collection = new ArrayList<>();
        collection.add(o1 = new SampleBooleanItem(true));
        collection.add(o2 = new SampleBooleanItem(true));
        collection.add(o3 = new SampleBooleanItem(true));
        collection.add(o4 = new SampleBooleanItem(true));
        collection.add(o5 = new SampleBooleanItem(true));

        collection.sort((i1, i2) -> c.compare(i1.getValue(), i2.getValue()));

        assertEquals(identityHashCode(o1), identityHashCode(collection.get(0)));
        assertEquals(identityHashCode(o2), identityHashCode(collection.get(1)));
        assertEquals(identityHashCode(o3), identityHashCode(collection.get(2)));
        assertEquals(identityHashCode(o4), identityHashCode(collection.get(3)));
        assertEquals(identityHashCode(o5), identityHashCode(collection.get(4)));
    }

    @Test
    public void sort___allTrue_DESC___noChanges() {
        c.setSortOrder(DESC);

        SampleBooleanItem o1, o2, o3, o4, o5;

        List<SampleBooleanItem> collection = new ArrayList<>();
        collection.add(o1 = new SampleBooleanItem(true));
        collection.add(o2 = new SampleBooleanItem(true));
        collection.add(o3 = new SampleBooleanItem(true));
        collection.add(o4 = new SampleBooleanItem(true));
        collection.add(o5 = new SampleBooleanItem(true));

        collection.sort((i1, i2) -> c.compare(i1.getValue(), i2.getValue()));

        assertEquals(identityHashCode(o1), identityHashCode(collection.get(0)));
        assertEquals(identityHashCode(o2), identityHashCode(collection.get(1)));
        assertEquals(identityHashCode(o3), identityHashCode(collection.get(2)));
        assertEquals(identityHashCode(o4), identityHashCode(collection.get(3)));
        assertEquals(identityHashCode(o5), identityHashCode(collection.get(4)));
    }

    @Test
    public void sort___allFalse_ASC___noChanges() {
        c.setSortOrder(ASC);

        SampleBooleanItem o1, o2, o3, o4, o5;

        List<SampleBooleanItem> collection = new ArrayList<>();
        collection.add(o1 = new SampleBooleanItem(false));
        collection.add(o2 = new SampleBooleanItem(false));
        collection.add(o3 = new SampleBooleanItem(false));
        collection.add(o4 = new SampleBooleanItem(false));
        collection.add(o5 = new SampleBooleanItem(false));

        collection.sort((i1, i2) -> c.compare(i1.getValue(), i2.getValue()));

        assertEquals(identityHashCode(o1), identityHashCode(collection.get(0)));
        assertEquals(identityHashCode(o2), identityHashCode(collection.get(1)));
        assertEquals(identityHashCode(o3), identityHashCode(collection.get(2)));
        assertEquals(identityHashCode(o4), identityHashCode(collection.get(3)));
        assertEquals(identityHashCode(o5), identityHashCode(collection.get(4)));
    }

    @Test
    public void sort___allFalse_DESC___noChanges() {
        c.setSortOrder(DESC);

        SampleBooleanItem o1, o2, o3, o4, o5;

        List<SampleBooleanItem> collection = new ArrayList<>();
        collection.add(o1 = new SampleBooleanItem(false));
        collection.add(o2 = new SampleBooleanItem(false));
        collection.add(o3 = new SampleBooleanItem(false));
        collection.add(o4 = new SampleBooleanItem(false));
        collection.add(o5 = new SampleBooleanItem(false));

        collection.sort((i1, i2) -> c.compare(i1.getValue(), i2.getValue()));

        assertEquals(identityHashCode(o1), identityHashCode(collection.get(0)));
        assertEquals(identityHashCode(o2), identityHashCode(collection.get(1)));
        assertEquals(identityHashCode(o3), identityHashCode(collection.get(2)));
        assertEquals(identityHashCode(o4), identityHashCode(collection.get(3)));
        assertEquals(identityHashCode(o5), identityHashCode(collection.get(4)));
    }

    @Test
    public void sort___mixedTrueFalse_ASC___sortedFalseThenTrue() {
        c.setSortOrder(ASC);

        SampleBooleanItem o1, o2, o3, o4, o5;

        List<SampleBooleanItem> collection = new ArrayList<>();
        collection.add(o1 = new SampleBooleanItem(false));
        collection.add(o2 = new SampleBooleanItem(true));
        collection.add(o3 = new SampleBooleanItem(false));
        collection.add(o4 = new SampleBooleanItem(true));
        collection.add(o5 = new SampleBooleanItem(false));

        collection.sort((i1, i2) -> c.compare(i1.getValue(), i2.getValue()));

        assertEquals(identityHashCode(o1), identityHashCode(collection.get(0)));
        assertEquals(identityHashCode(o3), identityHashCode(collection.get(1)));
        assertEquals(identityHashCode(o5), identityHashCode(collection.get(2)));
        assertEquals(identityHashCode(o2), identityHashCode(collection.get(3)));
        assertEquals(identityHashCode(o4), identityHashCode(collection.get(4)));
    }

    @Test
    public void sort___mixedTrueFalse_DESC___sortedFalseThenTrue() {
        c.setSortOrder(DESC);

        SampleBooleanItem o1, o2, o3, o4, o5;

        List<SampleBooleanItem> collection = new ArrayList<>();
        collection.add(o1 = new SampleBooleanItem(false));
        collection.add(o2 = new SampleBooleanItem(true));
        collection.add(o3 = new SampleBooleanItem(false));
        collection.add(o4 = new SampleBooleanItem(true));
        collection.add(o5 = new SampleBooleanItem(false));

        collection.sort((i1, i2) -> c.compare(i1.getValue(), i2.getValue()));

        assertEquals(identityHashCode(o2), identityHashCode(collection.get(0)));
        assertEquals(identityHashCode(o4), identityHashCode(collection.get(1)));
        assertEquals(identityHashCode(o1), identityHashCode(collection.get(2)));
        assertEquals(identityHashCode(o3), identityHashCode(collection.get(3)));
        assertEquals(identityHashCode(o5), identityHashCode(collection.get(4)));
    }

    @Test
    public void setSortOrder_sortOrderChanged() {
        c.setSortOrder(ASC);
        assertEquals(ASC, c.getSortOrder());

        c.setSortOrder(DESC);
        assertEquals(DESC, c.getSortOrder());
    }

}
