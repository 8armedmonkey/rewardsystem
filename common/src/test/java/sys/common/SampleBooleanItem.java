package sys.common;

public class SampleBooleanItem {

    private final boolean value;

    public SampleBooleanItem(boolean value) {
        this.value = value;
    }

    public boolean getValue() {
        return value;
    }

}
