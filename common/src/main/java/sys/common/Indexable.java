package sys.common;

public interface Indexable<T> {

    T getIndex();

}
