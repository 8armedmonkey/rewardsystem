package sys.common;

import java.io.Serializable;

@SuppressWarnings("WeakerAccess")
public abstract class Range<T extends Serializable> implements Serializable {

    private final T min;
    private final T max;

    public Range(T min, T max) {
        this.min = min;
        this.max = max;
    }

    public T getMin() {
        return min;
    }

    public T getMax() {
        return max;
    }

    public abstract boolean contains(T value);

}
