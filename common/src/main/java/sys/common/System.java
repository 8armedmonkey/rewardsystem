package sys.common;

import java.io.Serializable;

public interface System extends Serializable {

    void start();

    void stop();

}
