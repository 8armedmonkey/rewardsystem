package sys.common;

public interface OnError {

    void onError(Throwable t);

}
