package sys.common;

public class IntRange extends Range<Integer> {

    public IntRange(Integer min, Integer max) {
        super(min, max);
    }

    @Override
    public boolean contains(Integer value) {
        return getMin() <= value && getMax() >= value;
    }

}
