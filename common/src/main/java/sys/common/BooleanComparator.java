package sys.common;

import java.util.Comparator;

/**
 * Sort based on boolean value.
 * <p>
 * ASC: false then true.
 * DESC: true then false.
 */
public class BooleanComparator implements Comparator<Boolean> {

    private SortOrder sortOrder;

    public BooleanComparator() {
        this(SortOrder.ASC);
    }

    public BooleanComparator(SortOrder sortOrder) {
        this.sortOrder = sortOrder;
    }

    @Override
    public int compare(Boolean o1, Boolean o2) {
        boolean ascending = sortOrder == SortOrder.ASC;

        if (o1 == o2) return 0;
        else if (o1) return ascending ? 1 : -1;
        else return ascending ? -1 : 1;
    }

    public SortOrder getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(SortOrder sortOrder) {
        this.sortOrder = sortOrder;
    }

}
