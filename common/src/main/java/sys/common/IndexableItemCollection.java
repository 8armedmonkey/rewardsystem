package sys.common;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("WeakerAccess")
public abstract class IndexableItemCollection<T, IndexableItem extends Indexable<T> & Serializable>
        extends ItemCollection<IndexableItem> {

    private Map<T, IndexableItem> lookup;

    public IndexableItemCollection() {
        lookup = new HashMap<>();
    }

    @Override
    public synchronized void add(IndexableItem indexableItem) {
        if (!lookup.containsKey(indexableItem.getIndex())) {
            super.add(indexableItem);
            lookup.put(indexableItem.getIndex(), indexableItem);
        } else {
            String message = String.format(
                    "Item with index: %s already containsKey.",
                    indexableItem.getIndex().toString());

            throw new DuplicateItemException(message);
        }
    }

    @Override
    public synchronized void remove(IndexableItem indexableItem) {
        super.remove(indexableItem);
        lookup.remove(indexableItem.getIndex());
    }

    @Override
    public synchronized void remove(int position) {
        IndexableItem indexableItem = get(position);

        if (indexableItem != null) {
            super.remove(position);
            lookup.remove(indexableItem.getIndex());
        }
    }

    @Override
    public synchronized void clear() {
        super.clear();
        lookup.clear();
    }

    public synchronized void replace(IndexableItem indexableItem) {
        IndexableItem currentIndexableItem = lookup.get(indexableItem.getIndex());

        if (currentIndexableItem != null) {
            int position = items.indexOf(currentIndexableItem);

            items.set(position, indexableItem);
            lookup.put(indexableItem.getIndex(), indexableItem);
        } else {
            add(indexableItem);
        }
    }

    public synchronized IndexableItem getByIndex(T index) {
        return lookup.get(index);
    }
    
    public synchronized boolean containsKey(T index) {
        return lookup.containsKey(index);
    }

}
