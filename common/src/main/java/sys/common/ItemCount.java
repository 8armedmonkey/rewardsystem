package sys.common;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class ItemCount<T> {

    private Map<T, AtomicInteger> counts;

    public ItemCount() {
        counts = Collections.synchronizedMap(new HashMap<>());
    }

    public int get(T key) throws KeyNotExistsException {
        assertKeyExists(key);
        return counts.get(key).get();
    }

    public void set(T key, int count) {
        counts.put(key, new AtomicInteger(count));
    }

    public int increment(T key) throws KeyNotExistsException {
        assertKeyExists(key);
        return counts.get(key).incrementAndGet();
    }

    public int decrement(T key) throws KeyNotExistsException {
        assertKeyExists(key);
        return counts.get(key).decrementAndGet();
    }

    public int size() {
        return counts.size();
    }

    private void assertKeyExists(T key) throws KeyNotExistsException {
        if (!counts.containsKey(key)) {
            throw new KeyNotExistsException();
        }
    }

}
