package sys.common;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ItemCountUp<T extends Serializable> implements Serializable {

    protected Map<T, CountUp> counts;

    public ItemCountUp() {
        counts = Collections.synchronizedMap(new HashMap<>());
    }

    public int getCurrent(T key) {
        assertKeyExists(key);
        return counts.get(key).getCurrent();
    }

    public int getExpected(T key) {
        assertKeyExists(key);
        return counts.get(key).getExpected();
    }

    public void set(T key, int currentCount, int expectedCount) {
        counts.put(key, new CountUp(currentCount, expectedCount));
    }

    public int increment(T key) {
        assertKeyExists(key);
        return counts.get(key).increment();
    }

    public int size() {
        return counts.size();
    }

    public boolean hasReachedExpectedCounts() {
        boolean hasReachedExpectedCount = true;
        Iterator<CountUp> iterator = counts.values().iterator();

        while (hasReachedExpectedCount && iterator.hasNext()) {
            hasReachedExpectedCount = iterator.next().hasReachedExpected();
        }

        return hasReachedExpectedCount;
    }

    private void assertKeyExists(T key) throws KeyNotExistsException {
        if (!counts.containsKey(key)) {
            throw new KeyNotExistsException();
        }
    }

}
