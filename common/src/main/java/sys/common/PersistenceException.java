package sys.common;

public class PersistenceException extends Exception {

    public PersistenceException(Throwable cause) {
        super(cause);
    }

}
