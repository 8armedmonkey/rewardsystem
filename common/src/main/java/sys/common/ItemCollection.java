package sys.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public abstract class ItemCollection<Item extends Serializable>
        implements Iterable<Item>, Serializable {

    List<Item> items;

    public ItemCollection() {
        this.items = new ArrayList<>();
    }

    public ItemCollection(Item... items) {
        this.items = new ArrayList<>();

        if (items != null) {
            Collections.addAll(this.items, items);
        }
    }

    @Override
    public Iterator<Item> iterator() {
        return items.iterator();
    }

    public synchronized void add(Item item) {
        items.add(item);
    }

    public synchronized void addIfNotExists(Item item) {
        if (!items.contains(item)) {
            items.add(item);
        }
    }

    public synchronized void remove(Item item) {
        items.remove(item);
    }

    public synchronized void remove(int position) {
        items.remove(position);
    }

    public synchronized Item get(int position) {
        return items.get(position);
    }

    public synchronized void clear() {
        items.clear();
    }

    public synchronized int size() {
        return items.size();
    }

    public synchronized boolean contains(Item item) {
        return items.contains(item);
    }

}
