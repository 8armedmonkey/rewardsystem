package sys.common;

public class TimeRange extends Range<Long> {

    public TimeRange(Long min, Long max) {
        super(min, max);

        if (min >= max) {
            throw new IllegalArgumentException("Min value should be less than max value.");
        }
    }

    @Override
    public boolean contains(Long value) {
        return value >= getMin() && value <= getMax();
    }

}
