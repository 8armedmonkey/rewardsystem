package sys.common;

public interface Persistable {

    void save() throws PersistenceException;

    void restore() throws PersistenceException;

}
