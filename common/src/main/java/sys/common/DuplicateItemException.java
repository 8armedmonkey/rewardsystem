package sys.common;

@SuppressWarnings("WeakerAccess")
public class DuplicateItemException extends RuntimeException {

    public DuplicateItemException(String message) {
        super(message);
    }

}
