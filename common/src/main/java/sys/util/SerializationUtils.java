package sys.util;

import java.io.PrintWriter;
import java.io.StringWriter;

public class SerializationUtils {

    public static String toString(Throwable t) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);

        t.printStackTrace(pw);
        pw.close();

        return sw.toString();
    }

}
