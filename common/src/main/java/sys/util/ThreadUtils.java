package sys.util;

public class ThreadUtils {

    public static void interrupt(Thread t) {
        if (t != null) {
            t.interrupt();
        }
    }

}
