package sys.reward;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

public class RewardSpecTypeAdapter extends TypeAdapter<RewardSpec> {

    private static final String KEY_REWARD = "reward";
    private static final String KEY_REWARD_HAS_BEEN_CLAIMED = "rewardHasBeenClaimed";

    private RewardTypeAdapter rewardTypeAdapter;

    public RewardSpecTypeAdapter(RewardTypeAdapter rewardTypeAdapter) {
        this.rewardTypeAdapter = rewardTypeAdapter;
    }

    @Override
    public void write(JsonWriter out, RewardSpec value) throws IOException {
        out.beginObject();

        out.name(KEY_REWARD);
        rewardTypeAdapter.write(out, value.getReward());

        out.name(KEY_REWARD_HAS_BEEN_CLAIMED).value(value.hasRewardBeenClaimed());

        out.endObject();
    }

    @Override
    public RewardSpec read(JsonReader in) throws IOException {
        Reward reward = null;
        boolean rewardHasBeenClaimed = false;

        in.beginObject();

        while (in.hasNext()) {
            JsonToken token = in.peek();

            if (token == JsonToken.NAME) {
                String name = in.nextName();

                if (KEY_REWARD.equals(name)) {
                    reward = rewardTypeAdapter.read(in);

                } else if (KEY_REWARD_HAS_BEEN_CLAIMED.equals(name)) {
                    rewardHasBeenClaimed = in.nextBoolean();

                } else {
                    in.skipValue();

                }
            } else {
                in.skipValue();
            }
        }

        in.endObject();

        return new RewardSpec(reward, rewardHasBeenClaimed);
    }
}
