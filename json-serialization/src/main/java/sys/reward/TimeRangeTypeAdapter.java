package sys.reward;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import sys.common.TimeRange;

import java.io.IOException;

public class TimeRangeTypeAdapter extends TypeAdapter<TimeRange> {

    private static final String KEY_MIN = "min";
    private static final String KEY_MAX = "max";

    @Override
    public void write(JsonWriter out, TimeRange value) throws IOException {
        out.beginObject()
                .name(KEY_MIN).value(value.getMin())
                .name(KEY_MAX).value(value.getMax())
                .endObject();
    }

    @Override
    public TimeRange read(JsonReader in) throws IOException {
        long min = 0, max = 0;

        in.beginObject();

        while (in.hasNext()) {
            JsonToken token = in.peek();

            if (token == JsonToken.NAME) {
                String name = in.nextName();

                if (KEY_MIN.equals(name)) {
                    min = in.nextLong();
                } else if (KEY_MAX.equals(name)) {
                    max = in.nextLong();
                }
            }
        }

        in.endObject();

        return new TimeRange(min, max);
    }

}
