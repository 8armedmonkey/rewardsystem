package sys.reward;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import sys.event.Event;
import sys.event.EventCounts;

import java.io.IOException;

public class EventCountsTypeAdapter extends TypeAdapter<EventCounts> {

    private static final String KEY_CURRENT = "current";
    private static final String KEY_EXPECTED = "expected";

    @Override
    public void write(JsonWriter out, EventCounts value) throws IOException {
        out.beginObject();

        for (Class<? extends Event> eventClass : value.getEvents()) {
            out.name(eventClass.getName());
            out.beginObject()
                    .name(KEY_CURRENT).value(value.getCurrent(eventClass))
                    .name(KEY_EXPECTED).value(value.getExpected(eventClass))
                    .endObject();
        }

        out.endObject();
    }

    @SuppressWarnings("unchecked")
    @Override
    public EventCounts read(JsonReader in) throws IOException {
        EventCounts eventCounts = new EventCounts();

        in.beginObject();

        while (in.hasNext()) {
            JsonToken token = in.peek();

            if (token == JsonToken.NAME) {
                String name = in.nextName();

                try {
                    Class<? extends Event> eventClass =
                            (Class<? extends Event>) Class.forName(name);

                    readStatistics(in, eventCounts, eventClass);

                } catch (ClassNotFoundException e) {
                    in.skipValue();
                    e.printStackTrace();
                }
            }
        }

        in.endObject();

        return eventCounts;
    }

    private void readStatistics(
            JsonReader in,
            EventCounts eventCounts,
            Class<? extends Event> eventClass) throws IOException {

        int current = 0, expected = 0;

        in.beginObject();

        while (in.hasNext()) {
            String name = in.nextName();

            if (KEY_CURRENT.equals(name)) {
                current = in.nextInt();
            } else if (KEY_EXPECTED.equals(name)) {
                expected = in.nextInt();
            } else {
                in.skipValue();
            }
        }

        in.endObject();

        eventCounts.set(eventClass, current, expected);
    }

}
