package sys.reward;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import sys.event.Event;
import sys.event.EventClasses;

import java.io.IOException;

public class EventClassesTypeAdapter extends TypeAdapter<EventClasses> {

    @Override
    public void write(JsonWriter out, EventClasses value) throws IOException {
        out.beginArray();

        for (Class<? extends Event> eventClass : value) {
            out.value(eventClass.getName());
        }

        out.endArray();
    }

    @SuppressWarnings("unchecked")
    @Override
    public EventClasses read(JsonReader in) throws IOException {
        EventClasses eventClasses = new EventClasses();

        in.beginArray();

        while (in.hasNext()) {
            try {
                eventClasses.add((Class<? extends Event>) Class.forName(in.nextString()));
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        in.endArray();

        return eventClasses;
    }

}
