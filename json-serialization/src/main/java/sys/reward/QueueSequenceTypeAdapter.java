package sys.reward;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import sys.event.Event;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

public class QueueSequenceTypeAdapter extends TypeAdapter<Queue<Class<? extends Event>>> {

    @Override
    public void write(JsonWriter out, Queue<Class<? extends Event>> value) throws IOException {
        out.beginArray();

        Class[] array = value.toArray(new Class[0]);

        for (Class c : array) {
            out.value(c.getName());
        }

        out.endArray();
    }

    @SuppressWarnings("unchecked")
    @Override
    public Queue<Class<? extends Event>> read(JsonReader in) throws IOException {
        List<Class> list = new ArrayList<>();

        in.beginArray();

        while (in.hasNext()) {
            try {
                list.add(Class.forName(in.nextString()));
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        in.endArray();

        int capacity = Math.max(list.size(), 1);
        Queue<Class<? extends Event>> queue = new ArrayBlockingQueue<>(capacity, true);

        for (Class cls : list) {
            queue.offer(cls);
        }

        return queue;
    }

}
