package sys.reward;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import sys.event.Event;
import sys.event.EventClasses;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

public class ConditionTypeAdapter extends TypeAdapter<Condition> {

    private static final String KEY_INTERNAL_TYPE = "__type";
    private static final String KEY_SEQUENCE = "sequence";
    private static final String KEY_CONDITION_SPEC = "conditionSpec";
    private static final String KEY_REWARD_SPEC = "rewardSpec";
    private static final String KEY_EVENT_CLASSES = "eventClasses";

    private ConditionSpecTypeAdapter conditionSpecTypeAdapter;
    private RewardSpecTypeAdapter rewardSpecTypeAdapter;
    private EventClassesTypeAdapter eventClassesTypeAdapter;
    private QueueSequenceTypeAdapter queueSequenceTypeAdapter;

    public ConditionTypeAdapter(RewardTypeAdapter rewardTypeAdapter) {
        this.conditionSpecTypeAdapter = new ConditionSpecTypeAdapter();
        this.eventClassesTypeAdapter = new EventClassesTypeAdapter();
        this.rewardSpecTypeAdapter = new RewardSpecTypeAdapter(rewardTypeAdapter);
        this.queueSequenceTypeAdapter = new QueueSequenceTypeAdapter();
    }

    @Override
    public void write(JsonWriter out, Condition value) throws IOException {
        if (value instanceof BaseCondition) {
            write(out, (BaseCondition) value);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public Condition read(JsonReader in) throws IOException {
        Condition condition = null;

        in.beginObject();

        while (in.hasNext()) {
            JsonToken token = in.peek();

            if (token == JsonToken.NAME) {
                String name = in.nextName();

                if (KEY_INTERNAL_TYPE.equals(name)) {
                    String conditionClassName = in.nextString();

                    Map<String, Object> conditionAttributes = readConditionAttributes(in);
                    ConditionSpec conditionSpec = (ConditionSpec) conditionAttributes.get(KEY_CONDITION_SPEC);
                    RewardSpec rewardSpec = (RewardSpec) conditionAttributes.get(KEY_REWARD_SPEC);
                    EventClasses eventClasses = (EventClasses) conditionAttributes.get(KEY_EVENT_CLASSES);

                    if (TriggerCumulativeEventsCondition.class.getName().equals(conditionClassName)) {
                        condition = new TriggerCumulativeEventsCondition(
                                (TriggerCumulativeEventsConditionSpec) conditionSpec, rewardSpec);

                    } else if (TriggerEventsCombinationCondition.class.getName().equals(conditionClassName)) {
                        condition = new TriggerEventsCombinationCondition(
                                (TriggerEventsCombinationConditionSpec) conditionSpec, rewardSpec);

                    } else if (TriggerEventsInSequenceCondition.class.getName().equals(conditionClassName)) {
                        condition = new TriggerEventsInSequenceCondition(
                                (TriggerEventsInSequenceConditionSpec) conditionSpec, rewardSpec);

                        int sequenceSize =
                                ((TriggerEventsInSequenceConditionSpec) conditionSpec).getEventClasses().size();

                        Queue<Class<? extends Event>> sourceSequence =
                                (Queue<Class<? extends Event>>) conditionAttributes.get(KEY_SEQUENCE);

                        ((TriggerEventsInSequenceCondition) condition).sequence =
                                readSequenceFromAttribute(sequenceSize, sourceSequence);

                    } else if (TriggerNTimesCondition.class.getName().equals(conditionClassName)) {
                        condition = new TriggerNTimesCondition(
                                (TriggerNTimesConditionSpec) conditionSpec, rewardSpec);

                    } else if (TriggerWithinTimeLimitCondition.class.getName().equals(conditionClassName)) {
                        condition = new TriggerWithinTimeLimitCondition(
                                (TriggerWithinTimeLimitConditionSpec) conditionSpec, rewardSpec);

                    }

                    if (condition != null) {
                        for (Class<? extends Event> eventClass : eventClasses) {
                            condition.registerObservedEvent(eventClass);
                        }
                    }

                } else {
                    in.skipValue();
                }
            } else {
                in.skipValue();
            }
        }

        in.endObject();

        return condition;
    }

    private void write(JsonWriter out, BaseCondition value) throws IOException {
        out.beginObject();

        out.name(KEY_INTERNAL_TYPE).value(value.getClass().getName());

        if (TriggerEventsInSequenceCondition.class.equals(value.getClass())) {
            out.name(KEY_SEQUENCE);
            queueSequenceTypeAdapter.write(out, ((TriggerEventsInSequenceCondition) value).sequence);
        }

        out.name(KEY_CONDITION_SPEC);
        conditionSpecTypeAdapter.write(out, value.conditionSpec);

        out.name(KEY_REWARD_SPEC);
        rewardSpecTypeAdapter.write(out, value.rewardSpec);

        out.name(KEY_EVENT_CLASSES);
        eventClassesTypeAdapter.write(out, value.eventClasses);

        out.endObject();
    }

    private Map<String, Object> readConditionAttributes(JsonReader in) throws IOException {
        Map<String, Object> map = new HashMap<>();

        while (in.hasNext()) {
            JsonToken token = in.peek();

            if (token == JsonToken.NAME) {
                String name = in.nextName();

                if (KEY_CONDITION_SPEC.equals(name)) {
                    map.put(KEY_CONDITION_SPEC, conditionSpecTypeAdapter.read(in));

                } else if (KEY_REWARD_SPEC.equals(name)) {
                    map.put(KEY_REWARD_SPEC, rewardSpecTypeAdapter.read(in));

                } else if (KEY_EVENT_CLASSES.equals(name)) {
                    map.put(KEY_EVENT_CLASSES, eventClassesTypeAdapter.read(in));

                } else if (KEY_SEQUENCE.equals(name)) {
                    map.put(KEY_SEQUENCE, queueSequenceTypeAdapter.read(in));

                } else {
                    in.skipValue();

                }
            } else {
                in.skipValue();
            }
        }
        return map;
    }

    @SuppressWarnings("unchecked")
    private Queue<Class<? extends Event>> readSequenceFromAttribute(
            int sequenceSize, Queue<Class<? extends Event>> sourceSequence) {

        Queue<Class<? extends Event>> destinationSequence = new ArrayBlockingQueue<>(sequenceSize, true);
        Class[] classes = sourceSequence.toArray(new Class[0]);

        for (Class c : classes) {
            if (c != null) {
                destinationSequence.offer(c);
            }
        }

        return destinationSequence;
    }

}
