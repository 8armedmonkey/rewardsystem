package sys.reward;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import sys.common.CountUp;

import java.io.IOException;

public class CountUpTypeAdapter extends TypeAdapter<CountUp> {

    private static final String KEY_COUNT_UP_EXPECTED = "expected";
    private static final String KEY_COUNT_UP_CURRENT = "current";

    @Override
    public void write(JsonWriter out, CountUp value) throws IOException {
        out.beginObject()
                .name(KEY_COUNT_UP_EXPECTED).value(value.getExpected())
                .name(KEY_COUNT_UP_CURRENT).value(value.getCurrent())
                .endObject();
    }

    @Override
    public CountUp read(JsonReader in) throws IOException {
        int expected = 0, current = 0;

        in.beginObject();

        while (in.hasNext()) {
            JsonToken token = in.peek();

            if (token == JsonToken.NAME) {
                String name = in.nextName();

                if (KEY_COUNT_UP_EXPECTED.equals(name)) {
                    expected = in.nextInt();
                } else if (KEY_COUNT_UP_CURRENT.equals(name)) {
                    current = in.nextInt();
                } else {
                    in.skipValue();
                }
            } else {
                in.skipValue();
            }
        }

        in.endObject();

        return new CountUp(current, expected);
    }

}
