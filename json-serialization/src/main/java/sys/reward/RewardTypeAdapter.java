package sys.reward;

import com.google.gson.TypeAdapter;
import sys.reward.Reward;

@SuppressWarnings("WeakerAccess")
public abstract class RewardTypeAdapter extends TypeAdapter<Reward> {
}
