package sys.reward;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import sys.common.CountUp;
import sys.common.TimeRange;
import sys.event.EventClasses;
import sys.event.EventCounts;

import java.io.IOException;

@SuppressWarnings("WeakerAccess")
public class ConditionSpecTypeAdapter extends TypeAdapter<ConditionSpec> {

    private static final String KEY_INTERNAL_TYPE = "__type";

    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_DESCRIPTION = "description";
    private static final String KEY_HAS_BEEN_FULFILLED = "hasBeenFulfilled";
    private static final String KEY_FULFILLED_TIME_MILLIS = "fulfilledTimeMillis";

    private static final String KEY_COUNT_UP = "countUp";
    private static final String KEY_EVENT_COUNTS = "eventCounts";
    private static final String KEY_EVENT_CLASSES = "eventClasses";
    private static final String KEY_EVENT_OCCURRENCE_COUNT = "eventOccurrenceCount";
    private static final String KEY_EXPECTED_EVENT_OCCURRENCE_COUNT = "expectedEventOccurrenceCount";
    private static final String KEY_TIME_RANGE = "timeRange";

    private CountUpTypeAdapter countUpTypeAdapter;
    private EventCountsTypeAdapter eventCountsTypeAdapter;
    private EventClassesTypeAdapter eventClassesTypeAdapter;
    private TimeRangeTypeAdapter timeRangeTypeAdapter;

    public ConditionSpecTypeAdapter() {
        countUpTypeAdapter = new CountUpTypeAdapter();
        eventCountsTypeAdapter = new EventCountsTypeAdapter();
        eventClassesTypeAdapter = new EventClassesTypeAdapter();
        timeRangeTypeAdapter = new TimeRangeTypeAdapter();
    }

    @Override
    public void write(JsonWriter out, ConditionSpec value) throws IOException {
        if (value instanceof TriggerCumulativeEventsConditionSpec) {
            write(out, (TriggerCumulativeEventsConditionSpec) value);

        } else if (value instanceof TriggerEventsCombinationConditionSpec) {
            write(out, (TriggerEventsCombinationConditionSpec) value);

        } else if (value instanceof TriggerEventsInSequenceConditionSpec) {
            write(out, (TriggerEventsInSequenceConditionSpec) value);

        } else if (value instanceof TriggerNTimesConditionSpec) {
            write(out, (TriggerNTimesConditionSpec) value);

        } else if (value instanceof TriggerWithinTimeLimitConditionSpec) {
            write(out, (TriggerWithinTimeLimitConditionSpec) value);

        }
    }

    @Override
    public ConditionSpec read(JsonReader in) throws IOException {
        ConditionSpec conditionSpec = null;

        in.beginObject();

        while (in.hasNext()) {
            JsonToken token = in.peek();

            if (token == JsonToken.NAME) {
                String name = in.nextName();

                if (KEY_INTERNAL_TYPE.equals(name)) {
                    String conditionSpecClassName = in.nextString();

                    if (TriggerCumulativeEventsConditionSpec.class.getName().equals(conditionSpecClassName)) {
                        conditionSpec = readTriggerCumulativeEventsConditionSpec(in);

                    } else if (TriggerEventsCombinationConditionSpec.class.getName().equals(conditionSpecClassName)) {
                        conditionSpec = readTriggerEventsCombinationConditionSpec(in);

                    } else if (TriggerEventsInSequenceConditionSpec.class.getName().equals(conditionSpecClassName)) {
                        conditionSpec = readTriggerEventsInSequenceConditionSpec(in);

                    } else if (TriggerNTimesConditionSpec.class.getName().equals(conditionSpecClassName)) {
                        conditionSpec = readTriggerNTimesConditionSpec(in);

                    } else if (TriggerWithinTimeLimitConditionSpec.class.getName().equals(conditionSpecClassName)) {
                        conditionSpec = readTriggerWithinTimeLimitConditionSpec(in);

                    }
                }
            } else {
                in.skipValue();
            }
        }

        in.endObject();

        return conditionSpec;
    }

    private void writeConditionSpecAttributes(JsonWriter out, ConditionSpec value) throws IOException {
        out.name(KEY_ID).value(value.getId())
                .name(KEY_NAME).value(value.getName())
                .name(KEY_DESCRIPTION).value(value.getDescription())
                .name(KEY_HAS_BEEN_FULFILLED).value(value.hasBeenFulfilled())
                .name(KEY_FULFILLED_TIME_MILLIS).value(value.getFulfilledTimeMillis());
    }

    private boolean readConditionSpecAttributes(JsonReader in, String name, ConditionSpec.Builder builder) throws IOException {
        boolean read = false;

        if (KEY_ID.equals(name)) {
            builder.id(in.nextString());
            read = true;

        } else if (KEY_NAME.equals(name)) {
            builder.name(in.nextString());
            read = true;

        } else if (KEY_DESCRIPTION.equals(name)) {
            builder.description(in.nextString());
            read = true;

        } else if (KEY_HAS_BEEN_FULFILLED.equals(name)) {
            builder.hasBeenFulfilled(in.nextBoolean());
            read = true;

        } else if (KEY_FULFILLED_TIME_MILLIS.equals(name)) {
            builder.fulfilledTimeMillis(in.nextLong());
            read = true;

        }

        return read;
    }

    private void write(JsonWriter out, TriggerCumulativeEventsConditionSpec value) throws IOException {
        out.beginObject();

        out.name(KEY_INTERNAL_TYPE).value(TriggerCumulativeEventsConditionSpec.class.getName());

        out.name(KEY_COUNT_UP);
        countUpTypeAdapter.write(out, value.getCountUp());

        writeConditionSpecAttributes(out, value);

        out.endObject();
    }

    private ConditionSpec readTriggerCumulativeEventsConditionSpec(JsonReader in) throws IOException {
        TriggerCumulativeEventsConditionSpec.Builder builder = new TriggerCumulativeEventsConditionSpec.Builder();

        while (in.hasNext()) {
            JsonToken token = in.peek();

            if (token == JsonToken.NAME) {
                String name = in.nextName();

                if (KEY_COUNT_UP.equals(name)) {
                    CountUp countUp = countUpTypeAdapter.read(in);
                    builder.countUp(countUp);

                } else if (!readConditionSpecAttributes(in, name, builder)) {
                    in.skipValue();

                }
            } else {
                in.skipValue();
            }
        }

        return builder.build();
    }

    private void write(JsonWriter out, TriggerEventsCombinationConditionSpec value) throws IOException {
        out.beginObject();

        out.name(KEY_INTERNAL_TYPE).value(TriggerEventsCombinationConditionSpec.class.getName());

        out.name(KEY_EVENT_COUNTS);
        eventCountsTypeAdapter.write(out, value.getEventCounts());

        writeConditionSpecAttributes(out, value);

        out.endObject();
    }

    private ConditionSpec readTriggerEventsCombinationConditionSpec(JsonReader in) throws IOException {
        TriggerEventsCombinationConditionSpec.Builder builder = new TriggerEventsCombinationConditionSpec.Builder();

        while (in.hasNext()) {
            JsonToken token = in.peek();

            if (token == JsonToken.NAME) {
                String name = in.nextName();

                if (KEY_EVENT_COUNTS.equals(name)) {
                    EventCounts eventCounts = eventCountsTypeAdapter.read(in);
                    builder.eventCounts(eventCounts);

                } else if (!readConditionSpecAttributes(in, name, builder)) {
                    in.skipValue();

                }
            } else {
                in.skipValue();
            }
        }

        return builder.build();
    }

    private void write(JsonWriter out, TriggerEventsInSequenceConditionSpec value) throws IOException {
        out.beginObject();

        out.name(KEY_INTERNAL_TYPE).value(TriggerEventsInSequenceConditionSpec.class.getName());

        out.name(KEY_EVENT_CLASSES);
        eventClassesTypeAdapter.write(out, value.getEventClasses());

        writeConditionSpecAttributes(out, value);

        out.endObject();
    }

    private ConditionSpec readTriggerEventsInSequenceConditionSpec(JsonReader in) throws IOException {
        TriggerEventsInSequenceConditionSpec.Builder builder = new TriggerEventsInSequenceConditionSpec.Builder();

        while (in.hasNext()) {
            JsonToken token = in.peek();

            if (token == JsonToken.NAME) {
                String name = in.nextName();

                if (KEY_EVENT_CLASSES.equals(name)) {
                    EventClasses eventClasses = eventClassesTypeAdapter.read(in);
                    builder.eventClasses(eventClasses);

                } else if (!readConditionSpecAttributes(in, name, builder)) {
                    in.skipValue();

                }
            } else {
                in.skipValue();
            }
        }

        return builder.build();
    }

    private void write(JsonWriter out, TriggerNTimesConditionSpec value) throws IOException {
        out.beginObject();

        out.name(KEY_INTERNAL_TYPE).value(TriggerNTimesConditionSpec.class.getName());

        out.name(KEY_EVENT_OCCURRENCE_COUNT).value(value.getEventOccurrenceCount());
        out.name(KEY_EXPECTED_EVENT_OCCURRENCE_COUNT).value(value.getExpectedEventOccurrenceCount());

        writeConditionSpecAttributes(out, value);

        out.endObject();
    }

    private ConditionSpec readTriggerNTimesConditionSpec(JsonReader in) throws IOException {
        TriggerNTimesConditionSpec.Builder builder = new TriggerNTimesConditionSpec.Builder();

        while (in.hasNext()) {
            JsonToken token = in.peek();

            if (token == JsonToken.NAME) {
                String name = in.nextName();

                if (KEY_EVENT_OCCURRENCE_COUNT.equals(name)) {
                    builder.eventOccurrenceCount(in.nextInt());

                } else if (KEY_EXPECTED_EVENT_OCCURRENCE_COUNT.equals(name)) {
                    builder.expectedEventOccurrenceCount(in.nextInt());

                } else if (!readConditionSpecAttributes(in, name, builder)) {
                    in.skipValue();

                }
            } else {
                in.skipValue();
            }
        }
        return builder.build();
    }

    private void write(JsonWriter out, TriggerWithinTimeLimitConditionSpec value) throws IOException {
        out.beginObject();

        out.name(KEY_INTERNAL_TYPE).value(TriggerWithinTimeLimitConditionSpec.class.getName());

        out.name(KEY_TIME_RANGE);
        timeRangeTypeAdapter.write(out, value.getTimeRange());

        writeConditionSpecAttributes(out, value);

        out.endObject();
    }

    private ConditionSpec readTriggerWithinTimeLimitConditionSpec(JsonReader in) throws IOException {
        TriggerWithinTimeLimitConditionSpec.Builder builder = new TriggerWithinTimeLimitConditionSpec.Builder();

        while (in.hasNext()) {
            JsonToken token = in.peek();

            if (token == JsonToken.NAME) {
                String name = in.nextName();

                if (KEY_TIME_RANGE.equals(name)) {
                    TimeRange timeRange = timeRangeTypeAdapter.read(in);
                    builder.timeRange(timeRange);

                } else if (!readConditionSpecAttributes(in, name, builder)) {
                    in.skipValue();

                }
            } else {
                in.skipValue();
            }
        }
        return builder.build();
    }

}
