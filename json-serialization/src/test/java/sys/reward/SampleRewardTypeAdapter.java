package sys.reward;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

public class SampleRewardTypeAdapter extends RewardTypeAdapter {

    private static final String KEY_INTERNAL_TYPE = "__type";
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_DESCRIPTION = "description";

    @Override
    public void write(JsonWriter out, Reward value) throws IOException {
        out.beginObject()
                .name(KEY_INTERNAL_TYPE).value(value.getClass().getName())
                .name(KEY_ID).value(value.id())
                .name(KEY_NAME).value(value.name())
                .name(KEY_DESCRIPTION).value(value.description())
                .endObject();
    }

    @Override
    public Reward read(JsonReader in) throws IOException {
        String id = null, name = null, description = null;
        in.beginObject();

        while (in.hasNext()) {
            JsonToken token = in.peek();

            if (token == JsonToken.NAME) {
                String tokenName = in.nextName();

                if (KEY_ID.equals(tokenName)) {
                    id = in.nextString();

                } else if (KEY_NAME.equals(tokenName)) {
                    name = in.nextString();

                } else if (KEY_DESCRIPTION.equals(tokenName)) {
                    description = in.nextString();

                } else {
                    in.skipValue();

                }
            } else {
                in.skipValue();
            }
        }

        in.endObject();

        return new SampleReward(id, name, description);
    }

}
