package sys.reward;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.Before;
import org.junit.Test;
import sys.event.Event;

import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

import static org.junit.Assert.assertEquals;

public class TestQueueSequenceTypeAdapter {

    private Gson gson;

    @Before
    public void setUp() {
        gson = new GsonBuilder()
                .registerTypeHierarchyAdapter(Queue.class, new QueueSequenceTypeAdapter())
                .create();
    }

    @Test
    public void write() {
        Queue<Class<? extends Event>> sequence = new ArrayBlockingQueue<>(3, true);
        sequence.offer(SampleEvent2.class);
        sequence.offer(SampleEvent3.class);
        sequence.offer(SampleEvent1.class);

        String expected = "[" +
                "\"sys.reward.SampleEvent2\"," +
                "\"sys.reward.SampleEvent3\"," +
                "\"sys.reward.SampleEvent1\"" +
                "]";

        String actual = gson.toJson(sequence);

        assertEquals(expected, actual);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void read() {
        String json = "[" +
                "\"sys.reward.SampleEvent2\"," +
                "\"sys.reward.SampleEvent3\"," +
                "\"sys.reward.SampleEvent1\"" +
                "]";

        Queue<Class<? extends Event>> sequence =
                (Queue<Class<? extends Event>>) gson.fromJson(json, Queue.class);

        Class<? extends Event>[] array = sequence.toArray(new Class[0]);

        assertEquals(SampleEvent2.class, array[0]);
        assertEquals(SampleEvent3.class, array[1]);
        assertEquals(SampleEvent1.class, array[2]);
    }

}
