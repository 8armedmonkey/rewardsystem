package sys.reward;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.Before;
import org.junit.Test;
import sys.event.Event;
import sys.event.EventCounts;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestEventCountsTypeAdapter {

    private Gson gson;

    @Before
    public void setUp() {
        gson = new GsonBuilder()
                .registerTypeAdapter(EventCounts.class, new EventCountsTypeAdapter())
                .create();
    }

    @Test
    public void write() {
        EventCounts eventCounts = new EventCounts();
        eventCounts.set(Event.class, 1, 3);

        String expected = "{\"sys.event.Event\":{\"current\":1,\"expected\":3}}";
        String actual = gson.toJson(eventCounts);

        assertEquals(expected, actual);
    }

    @Test
    public void read() {
        String json = "{\"sys.event.Event\":{\"current\":1,\"expected\":3}}";
        EventCounts eventCounts = gson.fromJson(json, EventCounts.class);

        assertTrue(eventCounts.getEvents().contains(Event.class));
        assertEquals(1, eventCounts.getCurrent(Event.class));
        assertEquals(3, eventCounts.getExpected(Event.class));
    }

}
