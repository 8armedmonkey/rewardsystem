package sys.reward;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.Before;
import org.junit.Test;
import sys.common.CountUp;
import sys.common.TimeRange;
import sys.event.EventClasses;
import sys.event.EventCounts;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestConditionSpecTypeAdapter {

    private Gson gson;

    @Before
    public void setUp() {
        gson = new GsonBuilder()
                .registerTypeHierarchyAdapter(ConditionSpec.class, new ConditionSpecTypeAdapter())
                .create();
    }

    @Test
    public void writeTriggerCumulativeEventsConditionSpec() {
        TriggerCumulativeEventsConditionSpec spec = new TriggerCumulativeEventsConditionSpec.Builder()
                .id("Id")
                .name("Name")
                .description("Description")
                .hasBeenFulfilled(false)
                .fulfilledTimeMillis(0)
                .countUp(new CountUp(0, 3))
                .build();

        // @formatter:off
        String expected = "{" +
                "\"__type\":\"sys.reward.TriggerCumulativeEventsConditionSpec\"," +
                "\"countUp\":{\"expected\":3,\"current\":0}," +
                "\"id\":\"Id\"," +
                "\"name\":\"Name\"," +
                "\"description\":\"Description\"," +
                "\"hasBeenFulfilled\":false," +
                "\"fulfilledTimeMillis\":0" +
                "}";
        // @formatter:on

        String actual = gson.toJson(spec);

        assertEquals(expected, actual);
    }

    @Test
    public void readTriggerCumulativeEventsConditionSpec() {
        // @formatter:off
        String json = "{" +
                "\"__type\":\"sys.reward.TriggerCumulativeEventsConditionSpec\"," +
                "\"countUp\":{\"expected\":3,\"current\":0}," +
                "\"id\":\"Id\"," +
                "\"name\":\"Name\"," +
                "\"description\":\"Description\"," +
                "\"hasBeenFulfilled\":false," +
                "\"fulfilledTimeMillis\":0" +
                "}";
        // @formatter:on

        ConditionSpec spec = gson.fromJson(json, ConditionSpec.class);

        assertEquals(TriggerCumulativeEventsConditionSpec.class, spec.getClass());
        assertEquals(0, ((TriggerCumulativeEventsConditionSpec) spec).getCountUp().getCurrent());
        assertEquals(3, ((TriggerCumulativeEventsConditionSpec) spec).getCountUp().getExpected());
        assertEquals("Id", spec.getId());
        assertEquals("Name", spec.getName());
        assertEquals("Description", spec.getDescription());
        assertEquals(false, spec.hasBeenFulfilled());
        assertEquals(0, spec.getFulfilledTimeMillis());
    }

    @Test
    public void writeTriggerEventsCombinationConditionSpec() throws ClassNotFoundException {
        EventCounts eventCounts = new EventCounts();
        eventCounts.set(SampleEvent1.class, 0, 3);

        TriggerEventsCombinationConditionSpec spec = new TriggerEventsCombinationConditionSpec.Builder()
                .id("Id")
                .name("Name")
                .description("Description")
                .hasBeenFulfilled(false)
                .fulfilledTimeMillis(0)
                .eventCounts(eventCounts)
                .build();

        // @formatter:off
        String expected = "{" +
                "\"__type\":\"sys.reward.TriggerEventsCombinationConditionSpec\"," +
                "\"eventCounts\":{" +
                    "\"sys.reward.SampleEvent1\":{\"current\":0,\"expected\":3}" +
                "}," +
                "\"id\":\"Id\"," +
                "\"name\":\"Name\"," +
                "\"description\":\"Description\"," +
                "\"hasBeenFulfilled\":false," +
                "\"fulfilledTimeMillis\":0" +
                "}";
        // @formatter:on

        String actual = gson.toJson(spec);

        assertEquals(expected, actual);
    }

    @Test
    public void readTriggerEventsCombinationConditionSpec() {
        // @formatter:off
        String json = "{" +
                "\"__type\":\"sys.reward.TriggerEventsCombinationConditionSpec\"," +
                "\"eventCounts\":{" +
                    "\"sys.reward.SampleEvent1\":{\"current\":0,\"expected\":3}," +
                    "\"sys.reward.SampleEvent2\":{\"current\":1,\"expected\":3}," +
                    "\"sys.reward.SampleEvent3\":{\"current\":2,\"expected\":3}" +
                "}," +
                "\"id\":\"Id\"," +
                "\"name\":\"Name\"," +
                "\"description\":\"Description\"," +
                "\"hasBeenFulfilled\":false," +
                "\"fulfilledTimeMillis\":0" +
                "}";
        // @formatter:on

        ConditionSpec spec = gson.fromJson(json, ConditionSpec.class);

        assertEquals(TriggerEventsCombinationConditionSpec.class, spec.getClass());

        assertTrue(((TriggerEventsCombinationConditionSpec) spec).getEventCounts().getEvents().contains(SampleEvent1.class));
        assertTrue(((TriggerEventsCombinationConditionSpec) spec).getEventCounts().getEvents().contains(SampleEvent2.class));
        assertTrue(((TriggerEventsCombinationConditionSpec) spec).getEventCounts().getEvents().contains(SampleEvent3.class));

        assertEquals(0, ((TriggerEventsCombinationConditionSpec) spec).getEventCounts().getCurrent(SampleEvent1.class));
        assertEquals(3, ((TriggerEventsCombinationConditionSpec) spec).getEventCounts().getExpected(SampleEvent1.class));
        assertEquals(1, ((TriggerEventsCombinationConditionSpec) spec).getEventCounts().getCurrent(SampleEvent2.class));
        assertEquals(3, ((TriggerEventsCombinationConditionSpec) spec).getEventCounts().getExpected(SampleEvent2.class));
        assertEquals(2, ((TriggerEventsCombinationConditionSpec) spec).getEventCounts().getCurrent(SampleEvent3.class));
        assertEquals(3, ((TriggerEventsCombinationConditionSpec) spec).getEventCounts().getExpected(SampleEvent3.class));

        assertEquals("Id", spec.getId());
        assertEquals("Name", spec.getName());
        assertEquals("Description", spec.getDescription());
        assertEquals(false, spec.hasBeenFulfilled());
        assertEquals(0, spec.getFulfilledTimeMillis());
    }

    @Test
    public void writeTriggerEventsInSequenceConditionSpec() {
        EventClasses eventClasses = new EventClasses();
        eventClasses.add(SampleEvent2.class);
        eventClasses.add(SampleEvent1.class);
        eventClasses.add(SampleEvent3.class);

        TriggerEventsInSequenceConditionSpec spec = new TriggerEventsInSequenceConditionSpec.Builder()
                .id("Id")
                .name("Name")
                .description("Description")
                .hasBeenFulfilled(false)
                .fulfilledTimeMillis(0)
                .eventClasses(eventClasses)
                .build();

        // @formatter:off
        String expected = "{" +
                "\"__type\":\"sys.reward.TriggerEventsInSequenceConditionSpec\"," +
                "\"eventClasses\":[" +
                    "\"sys.reward.SampleEvent2\"," +
                    "\"sys.reward.SampleEvent1\"," +
                    "\"sys.reward.SampleEvent3\"" +
                "]," +
                "\"id\":\"Id\"," +
                "\"name\":\"Name\"," +
                "\"description\":\"Description\"," +
                "\"hasBeenFulfilled\":false," +
                "\"fulfilledTimeMillis\":0" +
                "}";
        // @formatter:on

        String actual = gson.toJson(spec);

        assertEquals(expected, actual);
    }

    @Test
    public void readTriggerEventsInSequenceConditionSpec() {
        // @formatter:off
        String json = "{" +
                "\"__type\":\"sys.reward.TriggerEventsInSequenceConditionSpec\"," +
                "\"eventClasses\":[" +
                    "\"sys.reward.SampleEvent2\"," +
                    "\"sys.reward.SampleEvent1\"," +
                    "\"sys.reward.SampleEvent3\"" +
                "]," +
                "\"id\":\"Id\"," +
                "\"name\":\"Name\"," +
                "\"description\":\"Description\"," +
                "\"hasBeenFulfilled\":false," +
                "\"fulfilledTimeMillis\":0" +
                "}";
        // @formatter:on

        ConditionSpec spec = gson.fromJson(json, ConditionSpec.class);

        assertEquals(TriggerEventsInSequenceConditionSpec.class, spec.getClass());

        assertEquals(SampleEvent2.class, ((TriggerEventsInSequenceConditionSpec) spec).getEventClasses().get(0));
        assertEquals(SampleEvent1.class, ((TriggerEventsInSequenceConditionSpec) spec).getEventClasses().get(1));
        assertEquals(SampleEvent3.class, ((TriggerEventsInSequenceConditionSpec) spec).getEventClasses().get(2));

        assertEquals("Id", spec.getId());
        assertEquals("Name", spec.getName());
        assertEquals("Description", spec.getDescription());
        assertEquals(false, spec.hasBeenFulfilled());
        assertEquals(0, spec.getFulfilledTimeMillis());
    }

    @Test
    public void writeTriggerNTimesConditionSpec() {
        TriggerNTimesConditionSpec spec = new TriggerNTimesConditionSpec.Builder()
                .id("Id")
                .name("Name")
                .description("Description")
                .hasBeenFulfilled(false)
                .fulfilledTimeMillis(0)
                .eventOccurrenceCount(3)
                .expectedEventOccurrenceCount(5)
                .build();

        // @formatter:off
        String expected = "{" +
                "\"__type\":\"sys.reward.TriggerNTimesConditionSpec\"," +
                "\"eventOccurrenceCount\":3," +
                "\"expectedEventOccurrenceCount\":5," +
                "\"id\":\"Id\"," +
                "\"name\":\"Name\"," +
                "\"description\":\"Description\"," +
                "\"hasBeenFulfilled\":false," +
                "\"fulfilledTimeMillis\":0" +
                "}";
        // @formatter:on

        String actual = gson.toJson(spec);

        assertEquals(expected, actual);
    }

    @Test
    public void readTriggerNTimesConditionSpec() {
        // @formatter:off
        String json = "{" +
                "\"__type\":\"sys.reward.TriggerNTimesConditionSpec\"," +
                "\"eventOccurrenceCount\":3," +
                "\"expectedEventOccurrenceCount\":5," +
                "\"id\":\"Id\"," +
                "\"name\":\"Name\"," +
                "\"description\":\"Description\"," +
                "\"hasBeenFulfilled\":false," +
                "\"fulfilledTimeMillis\":0" +
                "}";
        // @formatter:on

        ConditionSpec spec = gson.fromJson(json, ConditionSpec.class);

        assertEquals(TriggerNTimesConditionSpec.class, spec.getClass());

        assertEquals(3, ((TriggerNTimesConditionSpec) spec).getEventOccurrenceCount());
        assertEquals(5, ((TriggerNTimesConditionSpec) spec).getExpectedEventOccurrenceCount());

        assertEquals("Id", spec.getId());
        assertEquals("Name", spec.getName());
        assertEquals("Description", spec.getDescription());
        assertEquals(false, spec.hasBeenFulfilled());
        assertEquals(0, spec.getFulfilledTimeMillis());
    }

    @Test
    public void writeTriggerWithinTimeLimitConditionSpec() {
        TriggerWithinTimeLimitConditionSpec spec = new TriggerWithinTimeLimitConditionSpec.Builder()
                .id("Id")
                .name("Name")
                .description("Description")
                .hasBeenFulfilled(false)
                .fulfilledTimeMillis(0)
                .timeRange(new TimeRange(1000L, 3000L))
                .build();

        // @formatter:off
        String expected = "{" +
                "\"__type\":\"sys.reward.TriggerWithinTimeLimitConditionSpec\"," +
                "\"timeRange\":{\"min\":1000,\"max\":3000}," +
                "\"id\":\"Id\"," +
                "\"name\":\"Name\"," +
                "\"description\":\"Description\"," +
                "\"hasBeenFulfilled\":false," +
                "\"fulfilledTimeMillis\":0" +
                "}";
        // @formatter:on

        String actual = gson.toJson(spec);

        assertEquals(expected, actual);
    }

    @Test
    public void readTriggerWithinTimeLimitConditionSpec() {
        // @formatter:off
        String json = "{" +
                "\"__type\":\"sys.reward.TriggerWithinTimeLimitConditionSpec\"," +
                "\"timeRange\":{\"min\":1000,\"max\":3000}," +
                "\"id\":\"Id\"," +
                "\"name\":\"Name\"," +
                "\"description\":\"Description\"," +
                "\"hasBeenFulfilled\":false," +
                "\"fulfilledTimeMillis\":0" +
                "}";
        // @formatter:on

        ConditionSpec spec = gson.fromJson(json, ConditionSpec.class);

        assertEquals(TriggerWithinTimeLimitConditionSpec.class, spec.getClass());

        assertEquals(1000L, ((TriggerWithinTimeLimitConditionSpec) spec).getTimeRange().getMin().longValue());
        assertEquals(3000L, ((TriggerWithinTimeLimitConditionSpec) spec).getTimeRange().getMax().longValue());

        assertEquals("Id", spec.getId());
        assertEquals("Name", spec.getName());
        assertEquals("Description", spec.getDescription());
        assertEquals(false, spec.hasBeenFulfilled());
        assertEquals(0, spec.getFulfilledTimeMillis());
    }

}
