package sys.reward;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.Before;
import org.junit.Test;
import sys.common.TimeRange;

import static org.junit.Assert.assertEquals;

public class TestTimeRangeAdapter {

    private Gson gson;

    @Before
    public void setUp() {
        gson = new GsonBuilder()
                .registerTypeAdapter(TimeRange.class, new TimeRangeTypeAdapter())
                .create();
    }

    @Test
    public void write() {
        TimeRange timeRange = new TimeRange(1000L, 3000L);

        String expected = "{\"min\":1000,\"max\":3000}";
        String actual = gson.toJson(timeRange);

        assertEquals(expected, actual);
    }

    @Test
    public void read() {
        String json = "{\"min\":1000,\"max\":3000}";
        TimeRange timeRange = gson.fromJson(json, TimeRange.class);

        assertEquals(1000, timeRange.getMin().longValue());
        assertEquals(3000, timeRange.getMax().longValue());
    }

}
