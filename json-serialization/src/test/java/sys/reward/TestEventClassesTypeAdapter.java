package sys.reward;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.Before;
import org.junit.Test;
import sys.event.EventClasses;

import static org.junit.Assert.assertEquals;

public class TestEventClassesTypeAdapter {

    private Gson gson;

    @Before
    public void setUp() {
        gson = new GsonBuilder()
                .registerTypeAdapter(EventClasses.class, new EventClassesTypeAdapter())
                .create();
    }

    @Test
    public void write() {
        EventClasses eventClasses = new EventClasses();
        eventClasses.add(SampleEvent2.class);
        eventClasses.add(SampleEvent1.class);
        eventClasses.add(SampleEvent3.class);

        // @formatter:off
        String expected = "[" +
                "\"sys.reward.SampleEvent2\"," +
                "\"sys.reward.SampleEvent1\"," +
                "\"sys.reward.SampleEvent3\"" +
                "]";
        // @formatter:on

        String actual = gson.toJson(eventClasses);

        assertEquals(expected, actual);
    }

    @Test
    public void read() {
        // @formatter:off
        String json = "[" +
                "\"sys.reward.SampleEvent2\"," +
                "\"sys.reward.SampleEvent1\"," +
                "\"sys.reward.SampleEvent3\"" +
                "]";
        // @formatter:on

        EventClasses eventClasses = gson.fromJson(json, EventClasses.class);

        assertEquals(SampleEvent2.class, eventClasses.get(0));
        assertEquals(SampleEvent1.class, eventClasses.get(1));
        assertEquals(SampleEvent3.class, eventClasses.get(2));
    }

}
