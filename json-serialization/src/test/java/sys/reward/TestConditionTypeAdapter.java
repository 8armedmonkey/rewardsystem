package sys.reward;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.Before;
import org.junit.Test;
import sys.common.CountUp;
import sys.common.TimeRange;
import sys.event.EventClasses;
import sys.event.EventCounts;

import static org.junit.Assert.*;

public class TestConditionTypeAdapter {

    private Gson gson;

    @Before
    public void setUp() {
        ConditionTypeAdapter conditionTypeAdapter =
                new ConditionTypeAdapter(new SampleRewardTypeAdapter());

        gson = new GsonBuilder()
                .registerTypeHierarchyAdapter(Condition.class, conditionTypeAdapter)
                .create();
    }

    @Test
    public void writeTriggerCumulativeEventsCondition() {
        TriggerCumulativeEventsCondition condition = new TriggerCumulativeEventsCondition(
                new TriggerCumulativeEventsConditionSpec.Builder()
                        .id("id")
                        .name("name")
                        .description("description")
                        .hasBeenFulfilled(false)
                        .fulfilledTimeMillis(0)
                        .countUp(new CountUp(0, 3))
                        .build(),

                new RewardSpec(new SampleReward("id", "name", "description"), false));

        condition.registerObservedEvent(SampleCumulativeEvent.class);

        // @formatter:off
        String expected = "{" +
                    "\"__type\":\"sys.reward.TriggerCumulativeEventsCondition\"," +
                    "\"conditionSpec\":{" +
                        "\"__type\":\"sys.reward.TriggerCumulativeEventsConditionSpec\"," +
                        "\"countUp\":{\"expected\":3,\"current\":0}," +
                        "\"id\":\"id\"," +
                        "\"name\":\"name\"," +
                        "\"description\":\"description\"," +
                        "\"hasBeenFulfilled\":false," +
                        "\"fulfilledTimeMillis\":0" +
                    "}," +
                    "\"rewardSpec\":{" +
                        "\"reward\":{" +
                            "\"__type\":\"sys.reward.SampleReward\"," +
                            "\"id\":\"id\"," +
                            "\"name\":\"name\"," +
                            "\"description\":\"description\"" +
                        "}," +
                        "\"rewardHasBeenClaimed\":false" +
                    "}," +
                    "\"eventClasses\":[" +
                        "\"sys.reward.SampleCumulativeEvent\"" +
                    "]" +
                "}";
        // @formatter:on

        String actual = gson.toJson(condition);

        assertEquals(expected, actual);
    }

    @Test
    public void readTriggerCumulativeEventsCondition() throws Exception {
        // @formatter:off
        String json = "{" +
                    "\"__type\":\"sys.reward.TriggerCumulativeEventsCondition\"," +
                    "\"conditionSpec\":{" +
                        "\"__type\":\"sys.reward.TriggerCumulativeEventsConditionSpec\"," +
                        "\"countUp\":{\"expected\":3,\"current\":0}," +
                        "\"id\":\"id\"," +
                        "\"name\":\"name\"," +
                        "\"description\":\"description\"," +
                        "\"hasBeenFulfilled\":false," +
                        "\"fulfilledTimeMillis\":0" +
                    "}," +
                    "\"rewardSpec\":{" +
                        "\"reward\":{" +
                            "\"__type\":\"sys.reward.SampleReward\"," +
                            "\"id\":\"id\"," +
                            "\"name\":\"name\"," +
                            "\"description\":\"description\"" +
                        "}," +
                        "\"rewardHasBeenClaimed\":false" +
                    "}," +
                    "\"eventClasses\":[" +
                        "\"sys.reward.SampleCumulativeEvent\"" +
                    "]" +
                "}";
        // @formatter:on

        Condition condition = gson.fromJson(json, Condition.class);

        assertEquals(TriggerCumulativeEventsCondition.class, condition.getClass());

        ConditionSpec spec = ((BaseCondition) condition).conditionSpec;

        assertEquals(TriggerCumulativeEventsConditionSpec.class, spec.getClass());
        assertEquals(0, ((TriggerCumulativeEventsConditionSpec) spec).getCountUp().getCurrent());
        assertEquals(3, ((TriggerCumulativeEventsConditionSpec) spec).getCountUp().getExpected());
        assertEquals("id", spec.getId());
        assertEquals("name", spec.getName());
        assertEquals("description", spec.getDescription());
        assertFalse(spec.hasBeenFulfilled());
        assertEquals(0, spec.getFulfilledTimeMillis());

        assertEquals(SampleReward.class, condition.peekReward().getClass());
        assertEquals("id", condition.peekReward().id());
        assertEquals("name", condition.peekReward().name());
        assertEquals("description", condition.peekReward().description());
        assertFalse(condition.hasRewardBeenClaimed());

        assertTrue(condition.getObservedEventClasses().contains(SampleCumulativeEvent.class));
    }

    @Test
    public void writeTriggerEventsCombinationCondition() {
        EventCounts eventCounts = new EventCounts();
        eventCounts.set(SampleEvent2.class, 0, 5);
        eventCounts.set(SampleEvent3.class, 2, 3);

        TriggerEventsCombinationCondition condition = new TriggerEventsCombinationCondition(
                new TriggerEventsCombinationConditionSpec.Builder()
                        .id("id")
                        .name("name")
                        .description("description")
                        .hasBeenFulfilled(false)
                        .fulfilledTimeMillis(0)
                        .eventCounts(eventCounts)
                        .build(),

                new RewardSpec(new SampleReward("id", "name", "description"), false));

        condition.registerObservedEvent(SampleEvent2.class);
        condition.registerObservedEvent(SampleEvent3.class);

        // @formatter:off
        String expected = "{" +
                    "\"__type\":\"sys.reward.TriggerEventsCombinationCondition\"," +
                    "\"conditionSpec\":{" +
                        "\"__type\":\"sys.reward.TriggerEventsCombinationConditionSpec\"," +
                        "\"eventCounts\":{" +
                            "\"sys.reward.SampleEvent2\":{\"current\":0,\"expected\":5}," +
                            "\"sys.reward.SampleEvent3\":{\"current\":2,\"expected\":3}" +
                        "}," +
                        "\"id\":\"id\"," +
                        "\"name\":\"name\"," +
                        "\"description\":\"description\"," +
                        "\"hasBeenFulfilled\":false," +
                        "\"fulfilledTimeMillis\":0" +
                    "}," +
                    "\"rewardSpec\":{" +
                        "\"reward\":{" +
                            "\"__type\":\"sys.reward.SampleReward\"," +
                            "\"id\":\"id\"," +
                            "\"name\":\"name\"," +
                            "\"description\":\"description\"" +
                        "}," +
                        "\"rewardHasBeenClaimed\":false" +
                    "}," +
                    "\"eventClasses\":[" +
                        "\"sys.reward.SampleEvent2\"," +
                        "\"sys.reward.SampleEvent3\"" +
                    "]" +
                "}";
        // @formatter:on

        String actual = gson.toJson(condition);

        assertEquals(expected, actual);
    }

    @Test
    public void readTriggerEventsCombinationCondition() {
        // @formatter:off
        String json = "{" +
                    "\"__type\":\"sys.reward.TriggerEventsCombinationCondition\"," +
                    "\"conditionSpec\":{" +
                        "\"__type\":\"sys.reward.TriggerEventsCombinationConditionSpec\"," +
                        "\"eventCounts\":{" +
                            "\"sys.reward.SampleEvent2\":{\"current\":0,\"expected\":5}," +
                            "\"sys.reward.SampleEvent3\":{\"current\":2,\"expected\":3}" +
                        "}," +
                        "\"id\":\"id\"," +
                        "\"name\":\"name\"," +
                        "\"description\":\"description\"," +
                        "\"hasBeenFulfilled\":false," +
                        "\"fulfilledTimeMillis\":0" +
                    "}," +
                    "\"rewardSpec\":{" +
                        "\"reward\":{" +
                            "\"__type\":\"sys.reward.SampleReward\"," +
                            "\"id\":\"id\"," +
                            "\"name\":\"name\"," +
                            "\"description\":\"description\"" +
                        "}," +
                        "\"rewardHasBeenClaimed\":false" +
                    "}," +
                    "\"eventClasses\":[" +
                        "\"sys.reward.SampleEvent2\"," +
                        "\"sys.reward.SampleEvent3\"" +
                    "]" +
                "}";
        // @formatter:on

        Condition condition = gson.fromJson(json, Condition.class);

        assertEquals(TriggerEventsCombinationCondition.class, condition.getClass());

        ConditionSpec spec = ((BaseCondition) condition).conditionSpec;

        assertEquals(TriggerEventsCombinationConditionSpec.class, spec.getClass());
        assertTrue(((TriggerEventsCombinationConditionSpec) spec).getEventCounts().getEvents().contains(SampleEvent2.class));
        assertTrue(((TriggerEventsCombinationConditionSpec) spec).getEventCounts().getEvents().contains(SampleEvent3.class));

        assertEquals(0, ((TriggerEventsCombinationConditionSpec) spec).getEventCounts().getCurrent(SampleEvent2.class));
        assertEquals(5, ((TriggerEventsCombinationConditionSpec) spec).getEventCounts().getExpected(SampleEvent2.class));
        assertEquals(2, ((TriggerEventsCombinationConditionSpec) spec).getEventCounts().getCurrent(SampleEvent3.class));
        assertEquals(3, ((TriggerEventsCombinationConditionSpec) spec).getEventCounts().getExpected(SampleEvent3.class));

        assertEquals("id", spec.getId());
        assertEquals("name", spec.getName());
        assertEquals("description", spec.getDescription());
        assertFalse(spec.hasBeenFulfilled());
        assertEquals(0, spec.getFulfilledTimeMillis());

        assertEquals(SampleReward.class, condition.peekReward().getClass());
        assertEquals("id", condition.peekReward().id());
        assertEquals("name", condition.peekReward().name());
        assertEquals("description", condition.peekReward().description());
        assertFalse(condition.hasRewardBeenClaimed());

        assertTrue(condition.getObservedEventClasses().contains(SampleEvent2.class));
        assertTrue(condition.getObservedEventClasses().contains(SampleEvent3.class));
    }

    @Test
    public void writeTriggerEventsInSequenceCondition() {
        EventClasses eventClasses = new EventClasses();
        eventClasses.add(SampleEvent2.class);
        eventClasses.add(SampleEvent1.class);
        eventClasses.add(SampleEvent3.class);

        TriggerEventsInSequenceCondition condition = new TriggerEventsInSequenceCondition(
                new TriggerEventsInSequenceConditionSpec.Builder()
                        .id("id")
                        .name("name")
                        .description("description")
                        .hasBeenFulfilled(false)
                        .fulfilledTimeMillis(0)
                        .eventClasses(eventClasses)
                        .build(),

                new RewardSpec(new SampleReward("id", "name", "description"), false));

        condition.registerObservedEvent(SampleEvent2.class);
        condition.registerObservedEvent(SampleEvent1.class);
        condition.registerObservedEvent(SampleEvent3.class);

        condition.process(new SampleEvent2());
        condition.process(new SampleEvent3());

        // @formatter:off
        String expected = "{" +
                    "\"__type\":\"sys.reward.TriggerEventsInSequenceCondition\"," +
                    "\"sequence\":[\"sys.reward.SampleEvent2\",\"sys.reward.SampleEvent3\"]," +
                    "\"conditionSpec\":{" +
                        "\"__type\":\"sys.reward.TriggerEventsInSequenceConditionSpec\"," +
                        "\"eventClasses\":[" +
                            "\"sys.reward.SampleEvent2\"," +
                            "\"sys.reward.SampleEvent1\"," +
                            "\"sys.reward.SampleEvent3\"" +
                        "]," +
                        "\"id\":\"id\"," +
                        "\"name\":\"name\"," +
                        "\"description\":\"description\"," +
                        "\"hasBeenFulfilled\":false," +
                        "\"fulfilledTimeMillis\":0" +
                    "}," +
                    "\"rewardSpec\":{" +
                        "\"reward\":{" +
                            "\"__type\":\"sys.reward.SampleReward\"," +
                            "\"id\":\"id\"," +
                            "\"name\":\"name\"," +
                            "\"description\":\"description\"" +
                        "}," +
                        "\"rewardHasBeenClaimed\":false" +
                    "}," +
                    "\"eventClasses\":[" +
                        "\"sys.reward.SampleEvent2\"," +
                        "\"sys.reward.SampleEvent1\"," +
                        "\"sys.reward.SampleEvent3\"" +
                    "]" +
                "}";
        // @formatter:on

        String actual = gson.toJson(condition);

        assertEquals(expected, actual);
    }

    @Test
    public void readTriggerEventsInSequenceCondition() {
        // @formatter:off
        String json = "{" +
                    "\"__type\":\"sys.reward.TriggerEventsInSequenceCondition\"," +
                    "\"sequence\":[\"sys.reward.SampleEvent2\",\"sys.reward.SampleEvent3\"]," +
                    "\"conditionSpec\":{" +
                        "\"__type\":\"sys.reward.TriggerEventsInSequenceConditionSpec\"," +
                        "\"eventClasses\":[" +
                            "\"sys.reward.SampleEvent2\"," +
                            "\"sys.reward.SampleEvent1\"," +
                            "\"sys.reward.SampleEvent3\"" +
                        "]," +
                        "\"id\":\"id\"," +
                        "\"name\":\"name\"," +
                        "\"description\":\"description\"," +
                        "\"hasBeenFulfilled\":false," +
                        "\"fulfilledTimeMillis\":0" +
                    "}," +
                    "\"rewardSpec\":{" +
                        "\"reward\":{" +
                            "\"__type\":\"sys.reward.SampleReward\"," +
                            "\"id\":\"id\"," +
                            "\"name\":\"name\"," +
                            "\"description\":\"description\"" +
                        "}," +
                        "\"rewardHasBeenClaimed\":false" +
                    "}," +
                    "\"eventClasses\":[" +
                        "\"sys.reward.SampleEvent2\"," +
                        "\"sys.reward.SampleEvent1\"," +
                        "\"sys.reward.SampleEvent3\"" +
                    "]" +
                "}";
        // @formatter:on

        Condition condition = gson.fromJson(json, Condition.class);

        assertEquals(TriggerEventsInSequenceCondition.class, condition.getClass());

        Class[] sequence = ((TriggerEventsInSequenceCondition) condition).sequence.toArray(new Class[0]);
        assertEquals(SampleEvent2.class, sequence[0]);
        assertEquals(SampleEvent3.class, sequence[1]);

        ConditionSpec spec = ((BaseCondition) condition).conditionSpec;

        assertEquals(TriggerEventsInSequenceConditionSpec.class, spec.getClass());
        assertEquals(SampleEvent2.class, ((TriggerEventsInSequenceConditionSpec) spec).getEventClasses().get(0));
        assertEquals(SampleEvent1.class, ((TriggerEventsInSequenceConditionSpec) spec).getEventClasses().get(1));
        assertEquals(SampleEvent3.class, ((TriggerEventsInSequenceConditionSpec) spec).getEventClasses().get(2));

        assertEquals("id", spec.getId());
        assertEquals("name", spec.getName());
        assertEquals("description", spec.getDescription());
        assertFalse(spec.hasBeenFulfilled());
        assertEquals(0, spec.getFulfilledTimeMillis());

        assertEquals(SampleReward.class, condition.peekReward().getClass());
        assertEquals("id", condition.peekReward().id());
        assertEquals("name", condition.peekReward().name());
        assertEquals("description", condition.peekReward().description());
        assertFalse(condition.hasRewardBeenClaimed());

        assertTrue(condition.getObservedEventClasses().contains(SampleEvent2.class));
        assertTrue(condition.getObservedEventClasses().contains(SampleEvent1.class));
        assertTrue(condition.getObservedEventClasses().contains(SampleEvent3.class));
    }

    @Test
    public void writeTriggerNTimesCondition() {
        TriggerNTimesCondition condition = new TriggerNTimesCondition(
                new TriggerNTimesConditionSpec.Builder()
                        .id("id")
                        .name("name")
                        .description("description")
                        .hasBeenFulfilled(false)
                        .fulfilledTimeMillis(0)
                        .eventOccurrenceCount(3)
                        .expectedEventOccurrenceCount(5)
                        .build(),

                new RewardSpec(new SampleReward("id", "name", "description"), false));

        condition.registerObservedEvent(SampleEvent1.class);

        // @formatter:off
        String expected = "{" +
                    "\"__type\":\"sys.reward.TriggerNTimesCondition\"," +
                    "\"conditionSpec\":{" +
                        "\"__type\":\"sys.reward.TriggerNTimesConditionSpec\"," +
                        "\"eventOccurrenceCount\":3," +
                        "\"expectedEventOccurrenceCount\":5," +
                        "\"id\":\"id\"," +
                        "\"name\":\"name\"," +
                        "\"description\":\"description\"," +
                        "\"hasBeenFulfilled\":false," +
                        "\"fulfilledTimeMillis\":0" +
                    "}," +
                    "\"rewardSpec\":{" +
                        "\"reward\":{" +
                            "\"__type\":\"sys.reward.SampleReward\"," +
                            "\"id\":\"id\"," +
                            "\"name\":\"name\"," +
                            "\"description\":\"description\"" +
                        "}," +
                        "\"rewardHasBeenClaimed\":false" +
                    "}," +
                    "\"eventClasses\":[" +
                        "\"sys.reward.SampleEvent1\"" +
                    "]" +
                "}";
        // @formatter:on

        String actual = gson.toJson(condition);

        assertEquals(expected, actual);
    }

    @Test
    public void readTriggerNTimesCondition() {
        // @formatter:off
        String json = "{" +
                    "\"__type\":\"sys.reward.TriggerNTimesCondition\"," +
                    "\"conditionSpec\":{" +
                        "\"__type\":\"sys.reward.TriggerNTimesConditionSpec\"," +
                        "\"eventOccurrenceCount\":3," +
                        "\"expectedEventOccurrenceCount\":5," +
                        "\"id\":\"id\"," +
                        "\"name\":\"name\"," +
                        "\"description\":\"description\"," +
                        "\"hasBeenFulfilled\":false," +
                        "\"fulfilledTimeMillis\":0" +
                    "}," +
                    "\"rewardSpec\":{" +
                        "\"reward\":{" +
                            "\"__type\":\"sys.reward.SampleReward\"," +
                            "\"id\":\"id\"," +
                            "\"name\":\"name\"," +
                            "\"description\":\"description\"" +
                        "}," +
                        "\"rewardHasBeenClaimed\":false" +
                    "}," +
                    "\"eventClasses\":[" +
                        "\"sys.reward.SampleEvent1\"" +
                    "]" +
                "}";
        // @formatter:on

        Condition condition = gson.fromJson(json, Condition.class);

        assertEquals(TriggerNTimesCondition.class, condition.getClass());

        ConditionSpec spec = ((BaseCondition) condition).conditionSpec;

        assertEquals(TriggerNTimesConditionSpec.class, spec.getClass());
        assertEquals(3, ((TriggerNTimesConditionSpec) spec).getEventOccurrenceCount());
        assertEquals(5, ((TriggerNTimesConditionSpec) spec).getExpectedEventOccurrenceCount());

        assertEquals("id", spec.getId());
        assertEquals("name", spec.getName());
        assertEquals("description", spec.getDescription());
        assertFalse(spec.hasBeenFulfilled());
        assertEquals(0, spec.getFulfilledTimeMillis());

        assertEquals(SampleReward.class, condition.peekReward().getClass());
        assertEquals("id", condition.peekReward().id());
        assertEquals("name", condition.peekReward().name());
        assertEquals("description", condition.peekReward().description());
        assertFalse(condition.hasRewardBeenClaimed());

        assertTrue(condition.getObservedEventClasses().contains(SampleEvent1.class));
    }

    @Test
    public void writeTriggerWithinTimeLimitCondition() {
        TriggerWithinTimeLimitCondition condition = new TriggerWithinTimeLimitCondition(
                new TriggerWithinTimeLimitConditionSpec.Builder()
                        .id("id")
                        .name("name")
                        .description("description")
                        .hasBeenFulfilled(false)
                        .fulfilledTimeMillis(0)
                        .timeRange(new TimeRange(1000L, 3000L))
                        .build(),

                new RewardSpec(new SampleReward("id", "name", "description"), false));

        condition.registerObservedEvent(SampleEvent1.class);

        // @formatter:off
        String expected = "{" +
                    "\"__type\":\"sys.reward.TriggerWithinTimeLimitCondition\"," +
                    "\"conditionSpec\":{" +
                        "\"__type\":\"sys.reward.TriggerWithinTimeLimitConditionSpec\"," +
                        "\"timeRange\":{\"min\":1000,\"max\":3000}," +
                        "\"id\":\"id\"," +
                        "\"name\":\"name\"," +
                        "\"description\":\"description\"," +
                        "\"hasBeenFulfilled\":false," +
                        "\"fulfilledTimeMillis\":0" +
                    "}," +
                    "\"rewardSpec\":{" +
                        "\"reward\":{" +
                            "\"__type\":\"sys.reward.SampleReward\"," +
                            "\"id\":\"id\"," +
                            "\"name\":\"name\"," +
                            "\"description\":\"description\"" +
                        "}," +
                        "\"rewardHasBeenClaimed\":false" +
                    "}," +
                    "\"eventClasses\":[" +
                        "\"sys.reward.SampleEvent1\"" +
                    "]" +
                "}";
        // @formatter:on

        String actual = gson.toJson(condition);

        assertEquals(expected, actual);
    }

    @Test
    public void readTriggerWithinTimeLimitCondition() {
        // @formatter:off
        String json = "{" +
                    "\"__type\":\"sys.reward.TriggerWithinTimeLimitCondition\"," +
                    "\"conditionSpec\":{" +
                        "\"__type\":\"sys.reward.TriggerWithinTimeLimitConditionSpec\"," +
                        "\"timeRange\":{\"min\":1000,\"max\":3000}," +
                        "\"id\":\"id\"," +
                        "\"name\":\"name\"," +
                        "\"description\":\"description\"," +
                        "\"hasBeenFulfilled\":false," +
                        "\"fulfilledTimeMillis\":0" +
                    "}," +
                    "\"rewardSpec\":{" +
                        "\"reward\":{" +
                            "\"__type\":\"sys.reward.SampleReward\"," +
                            "\"id\":\"id\"," +
                            "\"name\":\"name\"," +
                            "\"description\":\"description\"" +
                        "}," +
                        "\"rewardHasBeenClaimed\":false" +
                    "}," +
                    "\"eventClasses\":[" +
                        "\"sys.reward.SampleEvent1\"" +
                    "]" +
                "}";
        // @formatter:on

        Condition condition = gson.fromJson(json, Condition.class);

        assertEquals(TriggerWithinTimeLimitCondition.class, condition.getClass());

        ConditionSpec spec = ((BaseCondition) condition).conditionSpec;

        assertEquals(TriggerWithinTimeLimitConditionSpec.class, spec.getClass());
        assertEquals(1000L, ((TriggerWithinTimeLimitConditionSpec) spec).getTimeRange().getMin().longValue());
        assertEquals(3000L, ((TriggerWithinTimeLimitConditionSpec) spec).getTimeRange().getMax().longValue());

        assertEquals("id", spec.getId());
        assertEquals("name", spec.getName());
        assertEquals("description", spec.getDescription());
        assertFalse(spec.hasBeenFulfilled());
        assertEquals(0, spec.getFulfilledTimeMillis());

        assertEquals(SampleReward.class, condition.peekReward().getClass());
        assertEquals("id", condition.peekReward().id());
        assertEquals("name", condition.peekReward().name());
        assertEquals("description", condition.peekReward().description());
        assertFalse(condition.hasRewardBeenClaimed());

        assertTrue(condition.getObservedEventClasses().contains(SampleEvent1.class));
    }

}
