package sys.reward;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.Before;
import org.junit.Test;
import sys.common.CountUp;

import static org.junit.Assert.assertEquals;

public class TestCountUpAdapter {

    private Gson gson;

    @Before
    public void setUp() {
        gson = new GsonBuilder()
                .registerTypeAdapter(CountUp.class, new CountUpTypeAdapter())
                .create();
    }

    @Test
    public void write() {
        CountUp countUp = new CountUp(0, 3);

        String expected = "{\"expected\":3,\"current\":0}";
        String actual = gson.toJson(countUp);

        assertEquals(expected, actual);
    }

    @Test
    public void read() {
        String json = "{\"expected\":3,\"current\":0}";

        CountUp countUp = gson.fromJson(json, CountUp.class);

        assertEquals(3, countUp.getExpected());
        assertEquals(0, countUp.getCurrent());
    }

}
