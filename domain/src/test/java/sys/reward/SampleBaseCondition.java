package sys.reward;

import sys.event.Event;

public class SampleBaseCondition extends BaseCondition {

    public SampleBaseCondition(ConditionSpec conditionSpec, RewardSpec rewardSpec) {
        super(conditionSpec, rewardSpec);
    }

    @Override
    public boolean process(Event event) {
        return hasBeenFulfilled();
    }

}
