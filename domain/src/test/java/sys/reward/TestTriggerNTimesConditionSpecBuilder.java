package sys.reward;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestTriggerNTimesConditionSpecBuilder {

    private TriggerNTimesConditionSpec.Builder builder;

    @Before
    public void setUp() {
        builder = new TriggerNTimesConditionSpec.Builder();
    }

    @Test
    public void expectedEventOccurrenceCount_expectedEventOccurrenceCountSetAndBuilderReturned() {
        assertEquals(builder, builder.expectedEventOccurrenceCount(3));
    }

    @Test
    public void eventOccurrenceCount_eventOccurrenceCountSetAndBuilderReturned() {
        assertEquals(builder, builder.eventOccurrenceCount(0));
    }

    @Test
    public void build_conditionSpecBuilt() {
        long currentTimeMillis = System.currentTimeMillis();

        TriggerNTimesConditionSpec conditionSpec = builder
                .id("id")
                .name("name")
                .description("description")
                .hasBeenFulfilled(false)
                .fulfilledTimeMillis(currentTimeMillis)
                .expectedEventOccurrenceCount(3)
                .eventOccurrenceCount(0)
                .build();

        assertEquals("id", conditionSpec.getId());
        assertEquals("name", conditionSpec.getName());
        assertEquals("description", conditionSpec.getDescription());
        assertEquals(false, conditionSpec.hasBeenFulfilled());
        assertEquals(currentTimeMillis, conditionSpec.getFulfilledTimeMillis());
        assertEquals(3, conditionSpec.getExpectedEventOccurrenceCount());
        assertEquals(0, conditionSpec.getEventOccurrenceCount());
    }

}
