package sys.reward;

import org.junit.Before;
import org.junit.Test;
import sys.event.EventClasses;

import static org.junit.Assert.assertEquals;

public class TestTriggerEventsInSequenceConditionSpecBuilder {

    private TriggerEventsInSequenceConditionSpec.Builder builder;

    @Before
    public void setUp() {
        builder = new TriggerEventsInSequenceConditionSpec.Builder();
    }

    @Test
    public void build_conditionSpecBuilt() {
        long currentTimeMillis = System.currentTimeMillis();

        EventClasses eventClasses = new EventClasses();
        eventClasses.add(SampleEvent1.class);
        eventClasses.add(SampleEvent2.class);
        eventClasses.add(SampleEvent3.class);

        TriggerEventsInSequenceConditionSpec conditionSpec = builder
                .id("id")
                .name("name")
                .description("description")
                .hasBeenFulfilled(false)
                .fulfilledTimeMillis(currentTimeMillis)
                .eventClasses(eventClasses)
                .build();

        assertEquals("id", conditionSpec.getId());
        assertEquals("name", conditionSpec.getName());
        assertEquals("description", conditionSpec.getDescription());
        assertEquals(false, conditionSpec.hasBeenFulfilled());
        assertEquals(currentTimeMillis, conditionSpec.getFulfilledTimeMillis());
        assertEquals(eventClasses, conditionSpec.getEventClasses());
    }

}
