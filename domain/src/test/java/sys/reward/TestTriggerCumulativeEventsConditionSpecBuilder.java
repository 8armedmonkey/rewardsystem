package sys.reward;

import org.junit.Before;
import org.junit.Test;
import sys.common.CountUp;

import static org.junit.Assert.assertEquals;

public class TestTriggerCumulativeEventsConditionSpecBuilder {

    private TriggerCumulativeEventsConditionSpec.Builder builder;

    @Before
    public void setUp() {
        builder = new TriggerCumulativeEventsConditionSpec.Builder();
    }

    @Test
    public void build_conditionSpecBuilt() {
        long currentTimeMillis = System.currentTimeMillis();
        CountUp countUp = new CountUp(0, 1);

        TriggerCumulativeEventsConditionSpec conditionSpec = builder
                .id("id")
                .name("name")
                .description("description")
                .hasBeenFulfilled(false)
                .fulfilledTimeMillis(currentTimeMillis)
                .countUp(countUp)
                .build();

        assertEquals("id", conditionSpec.getId());
        assertEquals("name", conditionSpec.getName());
        assertEquals("description", conditionSpec.getDescription());
        assertEquals(false, conditionSpec.hasBeenFulfilled());
        assertEquals(currentTimeMillis, conditionSpec.getFulfilledTimeMillis());
        assertEquals(countUp, conditionSpec.getCountUp());
    }

}
