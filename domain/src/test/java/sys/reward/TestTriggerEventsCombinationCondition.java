package sys.reward;

import org.junit.Before;
import org.junit.Test;
import sys.event.EventCounts;

import static java.lang.System.currentTimeMillis;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

public class TestTriggerEventsCombinationCondition {

    private TriggerEventsCombinationCondition condition;

    @Before
    public void setUp() {
        EventCounts eventCounts = new EventCounts();
        eventCounts.set(SampleEvent1.class, 0, 1);
        eventCounts.set(SampleEvent2.class, 0, 1);
        eventCounts.set(SampleEvent3.class, 0, 1);

        condition = new TriggerEventsCombinationCondition(
                new TriggerEventsCombinationConditionSpec.Builder()
                        .eventCounts(eventCounts)
                        .build(),
                new RewardSpec(mock(Reward.class), false));

        condition.registerObservedEvent(SampleEvent1.class);
        condition.registerObservedEvent(SampleEvent2.class);
        condition.registerObservedEvent(SampleEvent3.class);
    }

    @Test
    public void process_events_returnTrue_1() {
        assertFalse(condition.process(new SampleEvent2()));
        assertFalse(condition.process(new SampleEvent1()));
        assertTrue(condition.process(new SampleEvent3()));

        assertEquals(
                (double) currentTimeMillis(),
                (double) condition.getConditionFulfilledTimeMillis(),
                100
        );
    }

    @Test
    public void process_events_returnTrue_2() {
        assertFalse(condition.process(new SampleEvent3()));
        assertFalse(condition.process(new SampleEvent1()));
        assertTrue(condition.process(new SampleEvent2()));

        assertEquals(
                (double) currentTimeMillis(),
                (double) condition.getConditionFulfilledTimeMillis(),
                100
        );
    }

    @Test
    public void process_events_returnFalse() {
        assertFalse(condition.process(new SampleEvent2()));
        assertFalse(condition.process(new SampleEvent1()));
        assertFalse(condition.process(new SampleEvent1()));
        assertFalse(condition.process(new SampleEvent2()));
        assertFalse(condition.process(new SampleEvent2()));
    }

}
