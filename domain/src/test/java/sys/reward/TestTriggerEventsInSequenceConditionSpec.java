package sys.reward;

import org.junit.Test;
import sys.event.EventClasses;

public class TestTriggerEventsInSequenceConditionSpec {

    @Test(expected = IllegalArgumentException.class)
    public void constructor_emptyEventClasses_throwIllegalArgumentException() {
        new TriggerEventsInSequenceConditionSpec.Builder()
                .eventClasses(new EventClasses())
                .build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void setEventClasses_emptyEventClasses_throwIllegalArgumentException() {
        EventClasses eventClasses = new EventClasses();
        eventClasses.add(SampleEvent1.class);
        eventClasses.add(SampleEvent2.class);
        eventClasses.add(SampleEvent3.class);

        TriggerEventsInSequenceConditionSpec spec =
                new TriggerEventsInSequenceConditionSpec.Builder()
                        .eventClasses(eventClasses)
                        .build();

        spec.setEventClasses(new EventClasses());
    }

}
