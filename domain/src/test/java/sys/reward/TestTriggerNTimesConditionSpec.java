package sys.reward;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestTriggerNTimesConditionSpec {

    private TriggerNTimesConditionSpec conditionSpec;

    @Before
    public void setUp() {
        conditionSpec = new TriggerNTimesConditionSpec();
    }

    @Test
    public void setEventOccurrenceCount_eventOccurrenceCountSet() {
        conditionSpec.setEventOccurrenceCount(1);
        assertEquals(1, conditionSpec.getEventOccurrenceCount());
    }

    @Test
    public void setExpectedEventOccurrenceCount_expectedEventOccurrenceCountSet() {
        conditionSpec.setExpectedEventOccurrenceCount(5);
        assertEquals(5, conditionSpec.getExpectedEventOccurrenceCount());
    }

}
