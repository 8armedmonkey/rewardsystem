package sys.reward;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

public class TestBaseCondition {

    private ConditionSpec conditionSpec;
    private RewardSpec rewardSpec;
    private BaseCondition condition;

    @Before
    public void setUp() {
        conditionSpec = new ConditionSpec();
        conditionSpec.id = "id";
        conditionSpec.name = "name";
        conditionSpec.description = "description";
        conditionSpec.hasBeenFulfilled = false;
        conditionSpec.fulfilledTimeMillis = 0;

        rewardSpec = new RewardSpec(mock(Reward.class), false);

        condition = new SampleBaseCondition(conditionSpec, rewardSpec);
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = IllegalArgumentException.class)
    public void constructor_nullConditionSpec_exceptionThrown() {
        condition = new SampleBaseCondition(null, rewardSpec);
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = IllegalArgumentException.class)
    public void constructor_nullRewardSpec_exceptionThrown() {
        condition = new SampleBaseCondition(conditionSpec, null);
    }

    @Test
    public void registerObservedEvent_event_eventIsRegistered() {
        condition.registerObservedEvent(SampleEvent1.class);
        assertTrue(condition.getObservedEventClasses().contains(SampleEvent1.class));
    }

    @Test
    public void registerObservedEvent_multipleEvents_eventsAreRegistered() {
        condition.registerObservedEvent(SampleEvent1.class);
        condition.registerObservedEvent(SampleEvent2.class);

        assertTrue(condition.getObservedEventClasses().contains(SampleEvent1.class));
        assertTrue(condition.getObservedEventClasses().contains(SampleEvent2.class));
        assertFalse(condition.getObservedEventClasses().contains(SampleEvent3.class));
    }

    @Test
    public void unregisterObservedEvent_event_eventIsUnregistered() {
        condition.registerObservedEvent(SampleEvent1.class);
        condition.unregisterObservedEvent(SampleEvent1.class);

        assertFalse(condition.getObservedEventClasses().contains(SampleEvent1.class));
    }

    @Test
    public void unregisterObservedEvent_multipleEvents_eventIsUnregistered() {
        condition.registerObservedEvent(SampleEvent1.class);
        condition.registerObservedEvent(SampleEvent2.class);
        condition.registerObservedEvent(SampleEvent3.class);

        condition.unregisterObservedEvent(SampleEvent1.class);
        condition.unregisterObservedEvent(SampleEvent2.class);

        assertFalse(condition.getObservedEventClasses().contains(SampleEvent1.class));
        assertFalse(condition.getObservedEventClasses().contains(SampleEvent2.class));
        assertTrue(condition.getObservedEventClasses().contains(SampleEvent3.class));
    }

    @Test
    public void claimReward_conditionHasBeenFulfilled_returnReward() throws Exception {
        ((SampleBaseCondition) condition).conditionSpec.setHasBeenFulfilled(true);

        Reward reward = condition.claimReward();
        assertNotNull(reward);
    }

    @Test(expected = ConditionNotFulfilledException.class)
    public void claimReward_conditionHasNotBeenFulfilled_throwConditionNotFulfilledException() throws Exception {
        ((SampleBaseCondition) condition).conditionSpec.setHasBeenFulfilled(false);

        condition.claimReward();
    }

    @Test(expected = RewardHasBeenClaimedException.class)
    public void claimReward_rewardHasBeenClaimed_throwRewardHasBeenClaimedException() throws Exception {
        ((SampleBaseCondition) condition).conditionSpec.setHasBeenFulfilled(true);

        condition.claimReward();
        condition.claimReward();
    }

    @Test
    public void peekReward_conditionHasNotBeenFulfilled_returnRewardAndNoExceptionThrown() {
        ((SampleBaseCondition) condition).conditionSpec.setHasBeenFulfilled(false);

        Reward reward = condition.peekReward();
        assertNotNull(reward);
    }

    @Test
    public void peekReward_rewardHasBeenClaimed_returnRewardAndNoExceptionThrown() throws Exception {
        ((SampleBaseCondition) condition).conditionSpec.setHasBeenFulfilled(true);

        condition.claimReward();

        Reward reward = condition.peekReward();
        assertNotNull(reward);
    }

    @Test
    public void getIndex_indexReturned() {
        assertEquals("id", condition.getIndex());
    }

    @Test
    public void id_idReturned() {
        assertEquals("id", condition.id());
    }

    @Test
    public void name_nameReturned() {
        assertEquals("name", condition.name());
    }

    @Test
    public void description_descriptionReturned() {
        assertEquals("description", condition.description());
    }

    @Test
    public void hasBeenFulfilled_hasBeenFulfilledReturned() {
        assertEquals(false, condition.hasBeenFulfilled());
    }

    @Test
    public void fulfilledTimeMillis_fulfilledTimeMillisReturned() throws Exception {
        ((SampleBaseCondition) condition).conditionSpec.setHasBeenFulfilled(true);
        assertEquals(0L, condition.getConditionFulfilledTimeMillis());
    }

}
