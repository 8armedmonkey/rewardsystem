package sys.reward;

import org.junit.Before;
import org.junit.Test;
import sys.common.CountUp;

import static java.lang.System.currentTimeMillis;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

public class TestTriggerCumulativeEventsCondition {

    private TriggerCumulativeEventsCondition condition;

    @Before
    public void setUp() {
        condition = new TriggerCumulativeEventsCondition(
                new TriggerCumulativeEventsConditionSpec.Builder()
                        .countUp(new CountUp(0, 5))
                        .build(),
                new RewardSpec(mock(Reward.class), false));

        condition.registerObservedEvent(SampleCumulativeEvent1.class);
        condition.registerObservedEvent(SampleCumulativeEvent2.class);
        condition.registerObservedEvent(SampleCumulativeEvent3.class);
    }

    @Test
    public void registerObservedEvent_eventInstanceOfCumulativeEvent_eventRegistered() {
        condition.registerObservedEvent(SampleCumulativeEvent1.class);
        assertTrue(condition.getObservedEventClasses().contains(SampleCumulativeEvent1.class));
    }

    @Test(expected = IllegalArgumentException.class)
    public void registerObservedEvent_eventNotInstanceOfCumulativeEvent_throwIllegalArgumentException() {
        condition.registerObservedEvent(SampleEvent1.class);
    }

    @Test
    public void process_events_returnTrue() {
        assertFalse(condition.process(new SampleCumulativeEvent1(1)));
        assertFalse(condition.process(new SampleCumulativeEvent2(2)));
        assertFalse(condition.process(new SampleCumulativeEvent3(1)));
        assertTrue(condition.process(new SampleCumulativeEvent1(3)));

        assertEquals(
                (double) currentTimeMillis(),
                (double) condition.getConditionFulfilledTimeMillis(),
                100
        );
    }

}
