package sys.reward;

import org.junit.Before;
import org.junit.Test;
import sys.event.EventCounts;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestEventCounts {

    private EventCounts eventCounts;

    @Before
    public void setUp() {
        eventCounts = new EventCounts();
        eventCounts.set(SampleEvent1.class, 0, 1);
        eventCounts.set(SampleEvent2.class, 1, 1);
    }

    @Test
    public void increment_registeredEventClass_countIncremented() {
        eventCounts.increment(SampleEvent1.class);
        assertEquals(1, eventCounts.getCurrent(SampleEvent1.class));
    }

    @Test
    public void getEvents_registeredEventClassesReturned() {
        assertTrue(eventCounts.getEvents().contains(SampleEvent1.class));
        assertTrue(eventCounts.getEvents().contains(SampleEvent2.class));
        assertFalse(eventCounts.getEvents().contains(SampleEvent3.class));
    }

}
