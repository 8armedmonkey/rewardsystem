package sys.reward;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import sys.event.Event;
import sys.event.EventBroadcaster;
import sys.event.EventHandler;

import java.util.Iterator;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class TestRewardSystem {

    private ConditionRepository conditionRepository;
    private EventBroadcaster eventBroadcaster;
    private EventBroadcaster.Subscription eventSubscription;
    private RewardSystem rewardSystem;

    @SuppressWarnings("unchecked")
    @Before
    public void setUp() {
        conditionRepository = mock(ConditionRepository.class);
        eventBroadcaster = mock(EventBroadcaster.class);
        eventSubscription = mock(EventBroadcaster.Subscription.class);
        rewardSystem = new RewardSystemImpl(conditionRepository, eventBroadcaster);

        when(eventBroadcaster.subscribe(any(Class.class), any(EventHandler.class)))
                .thenReturn(eventSubscription);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void start_subscribedToEvents() {
        rewardSystem.start();
        verify(eventBroadcaster).subscribe(any(Class.class), any(EventHandler.class));
    }

    @Test
    public void stop_unsubscribeFromEvents() {
        rewardSystem.start();
        rewardSystem.stop();

        verify(eventSubscription).unsubscribe();
    }

    @Test
    public void save_stateSavedToRepository() throws Exception {
        rewardSystem.save();
        verify(conditionRepository).store(any(Conditions.class));
    }

    @Test
    public void restore_stateRestoredToRepository() throws Exception {
        rewardSystem.restore();
        verify(conditionRepository).retrieve();
    }

    @Test
    public void getConditions_returnIteratorForConditions() {
        Condition condition1 = mock(Condition.class);
        Condition condition2 = mock(Condition.class);

        when(condition1.getIndex()).thenReturn("1");
        when(condition2.getIndex()).thenReturn("2");

        rewardSystem.registerCondition(condition1);
        rewardSystem.registerCondition(condition2);

        Iterator<Condition> iterator = rewardSystem.getConditions().iterator();

        assertEquals(condition1, iterator.next());
        assertEquals(condition2, iterator.next());
    }

    @Test
    public void registerCondition_conditionIsRegistered() {
        Condition condition = mock(Condition.class);
        rewardSystem.registerCondition(condition);

        assertTrue(rewardSystem.contains(condition));
    }

    @Test
    public void unregisterCondition_conditionIsUnregistered() {
        Condition condition = mock(Condition.class);
        rewardSystem.registerCondition(condition);
        rewardSystem.unregisterCondition(condition);

        assertFalse(rewardSystem.contains(condition));
    }

    @Test
    public void onEvent_event_fulfilledConditionsNotProcessEvent() {
        Condition condition1 = mock(Condition.class);
        Condition condition2 = mock(Condition.class);
        Condition condition3 = mock(Condition.class);

        when(condition1.getIndex()).thenReturn("1");
        when(condition2.getIndex()).thenReturn("2");
        when(condition3.getIndex()).thenReturn("3");

        when(condition1.hasBeenFulfilled()).thenReturn(true);
        when(condition2.hasBeenFulfilled()).thenReturn(true);
        when(condition3.hasBeenFulfilled()).thenReturn(false);

        rewardSystem.registerCondition(condition1);
        rewardSystem.registerCondition(condition2);
        rewardSystem.registerCondition(condition3);

        ((RewardSystemImpl) rewardSystem).onEvent(new SampleEvent1());

        verify(condition1, never()).process(any(Event.class));
        verify(condition2, never()).process(any(Event.class));
    }

    @Test
    public void onEvent_event_unfulfilledConditionsProcessEvent() {
        Condition condition1 = mock(Condition.class);
        Condition condition2 = mock(Condition.class);
        Condition condition3 = mock(Condition.class);

        when(condition1.getIndex()).thenReturn("1");
        when(condition2.getIndex()).thenReturn("2");
        when(condition3.getIndex()).thenReturn("3");

        when(condition1.hasBeenFulfilled()).thenReturn(false);
        when(condition2.hasBeenFulfilled()).thenReturn(false);
        when(condition3.hasBeenFulfilled()).thenReturn(true);

        rewardSystem.registerCondition(condition1);
        rewardSystem.registerCondition(condition2);
        rewardSystem.registerCondition(condition3);

        ((RewardSystemImpl) rewardSystem).onEvent(new SampleEvent1());

        verify(condition1).process(any(Event.class));
        verify(condition2).process(any(Event.class));
    }

    @Test
    public void onEvent_conditionFulfilled_eventBroadcasted() {
        Condition condition1 = mock(Condition.class);
        Condition condition2 = mock(Condition.class);
        Condition condition3 = mock(Condition.class);

        when(condition1.getIndex()).thenReturn("1");
        when(condition2.getIndex()).thenReturn("2");
        when(condition3.getIndex()).thenReturn("3");

        when(condition1.process(any())).thenReturn(true);
        when(condition2.process(any())).thenReturn(true);
        when(condition3.process(any())).thenReturn(false);

        rewardSystem.registerCondition(condition1);
        rewardSystem.registerCondition(condition2);
        rewardSystem.registerCondition(condition3);

        ((RewardSystemImpl) rewardSystem).onEvent(new SampleEvent1());

        ArgumentCaptor<Event> captor = ArgumentCaptor.forClass(Event.class);
        verify(eventBroadcaster, times(2)).broadcast(captor.capture());

        Event event1 = captor.getAllValues().get(0);
        Event event2 = captor.getAllValues().get(1);

        assertEquals(ConditionFulfilled.class, event1.getClass());
        assertEquals(condition1, ((ConditionFulfilled) event1).getCondition());

        assertEquals(ConditionFulfilled.class, event2.getClass());
        assertEquals(condition2, ((ConditionFulfilled) event2).getCondition());
    }

}
