package sys.reward;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

public class TestRewardSpec {

    private RewardSpec rewardSpec;

    @Before
    public void setUp() {
        rewardSpec = new RewardSpec(mock(Reward.class), false);
    }

    @Test
    public void setReward_rewardSet() {
        Reward newReward = mock(Reward.class);
        rewardSpec.setReward(newReward);

        assertEquals(newReward, rewardSpec.getReward());
    }

}
