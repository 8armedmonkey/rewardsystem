package sys.reward;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestConditionSpecBuilder {

    private ConditionSpec.Builder builder;

    @Before
    public void setUp() {
        builder = new SampleConditionSpecBuilder();
    }

    @Test
    public void id_idSetAndBuilderReturned() {
        assertEquals(builder, builder.id("id"));
    }

    @Test
    public void name_nameSetAndBuilderReturned() {
        assertEquals(builder, builder.id("name"));
    }

    @Test
    public void description_descriptionSetAndBuilderReturned() {
        assertEquals(builder, builder.id("description"));
    }

    @Test
    public void hasBeenFulfilled_hasBeenFulfilledSetAndBuilderReturned() {
        assertEquals(builder, builder.hasBeenFulfilled(false));
    }

    @Test
    public void fulfilledTimeMillis_fulfilledTimeMillisSetAndBuilderReturned() {
        assertEquals(builder, builder.fulfilledTimeMillis(System.currentTimeMillis()));
    }

    @Test
    public void build_conditionSpecBuilt() {
        long currentTimeMillis = System.currentTimeMillis();

        ConditionSpec conditionSpec = builder
                .id("id")
                .name("name")
                .description("description")
                .hasBeenFulfilled(false)
                .fulfilledTimeMillis(currentTimeMillis)
                .build();

        assertEquals("id", conditionSpec.getId());
        assertEquals("name", conditionSpec.getName());
        assertEquals("description", conditionSpec.getDescription());
        assertEquals(false, conditionSpec.hasBeenFulfilled());
        assertEquals(currentTimeMillis, conditionSpec.getFulfilledTimeMillis());
    }

}
