package sys.reward;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestConditionSpec {

    private ConditionSpec conditionSpec;

    @Before
    public void setUp() {
        conditionSpec = new ConditionSpec();
    }

    @Test
    public void setId_idSet() {
        conditionSpec.setId("id");
        assertEquals("id", conditionSpec.getId());
    }

    @Test
    public void setName_nameSet() {
        conditionSpec.setName("name");
        assertEquals("name", conditionSpec.getName());
    }

    @Test
    public void setDescription_descriptionSet() {
        conditionSpec.setDescription("description");
        assertEquals("description", conditionSpec.getDescription());
    }

    @Test
    public void setHasBeenFulfilled_hasBeenFulfilledSet() {
        conditionSpec.setHasBeenFulfilled(false);
        assertEquals(false, conditionSpec.hasBeenFulfilled());
    }

    @Test
    public void setFulfilledTimeMillis_fulfilledTimeMillisSet() {
        long currentTimeMillis = System.currentTimeMillis();

        conditionSpec.setFulfilledTimeMillis(currentTimeMillis);
        assertEquals(currentTimeMillis, conditionSpec.getFulfilledTimeMillis());
    }

}
