package sys.reward;

import org.junit.Before;
import org.junit.Test;
import sys.event.EventCounts;

import static org.junit.Assert.assertEquals;

public class TestTriggerEventsCombinationConditionSpecBuilder {

    private TriggerEventsCombinationConditionSpec.Builder builder;

    @Before
    public void setUp() {
        builder = new TriggerEventsCombinationConditionSpec.Builder();
    }

    @Test
    public void build_conditionSpecBuilt() {
        long currentTimeMillis = System.currentTimeMillis();

        EventCounts eventCounts = new EventCounts();
        eventCounts.set(SampleEvent1.class, 0, 1);
        eventCounts.set(SampleEvent2.class, 1, 1);
        eventCounts.set(SampleEvent3.class, 0, 1);

        TriggerEventsCombinationConditionSpec conditionSpec = builder
                .id("id")
                .name("name")
                .description("description")
                .hasBeenFulfilled(false)
                .fulfilledTimeMillis(currentTimeMillis)
                .eventCounts(eventCounts)
                .build();

        assertEquals("id", conditionSpec.getId());
        assertEquals("name", conditionSpec.getName());
        assertEquals("description", conditionSpec.getDescription());
        assertEquals(false, conditionSpec.hasBeenFulfilled());
        assertEquals(currentTimeMillis, conditionSpec.getFulfilledTimeMillis());
        assertEquals(eventCounts, conditionSpec.getEventCounts());
    }

}
