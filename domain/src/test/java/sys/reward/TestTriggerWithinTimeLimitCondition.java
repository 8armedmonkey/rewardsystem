package sys.reward;

import org.junit.Before;
import org.junit.Test;
import sys.common.TimeRange;

import static java.lang.System.currentTimeMillis;
import static java.lang.Thread.sleep;
import static org.hamcrest.Matchers.closeTo;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

public class TestTriggerWithinTimeLimitCondition {

    private TriggerWithinTimeLimitCondition condition;

    @Before
    public void setUp() {
        condition = new TriggerWithinTimeLimitCondition(
                new TriggerWithinTimeLimitConditionSpec.Builder()
                        .timeRange(new TimeRange(
                                currentTimeMillis() - 1000,
                                currentTimeMillis() + 1000
                        ))
                        .build(),
                new RewardSpec(mock(Reward.class), false));
    }

    @Test
    public void process_eventWithinTimeRange_returnTrue() {
        condition.registerObservedEvent(SampleEvent1.class);
        assertTrue(condition.process(new SampleEvent1()));
    }

    @Test
    public void process_eventOutOfTimeRange_returnFalse() throws Exception {
        condition.registerObservedEvent(SampleEvent1.class);
        sleep(2000);
        assertFalse(condition.process(new SampleEvent1()));
    }

    @Test
    public void process_eventWithinTimeRange_conditionFulfilledTimeRecorded() throws Exception {
        condition.registerObservedEvent(SampleEvent1.class);
        sleep(500);

        condition.process(new SampleEvent1());
        assertThat((double) condition.getConditionFulfilledTimeMillis(), closeTo(currentTimeMillis(), 100));
    }

    @Test
    public void getTimeRange_timeRangeReturned() {
        assertNotNull(condition.getTimeRange());
    }

}
