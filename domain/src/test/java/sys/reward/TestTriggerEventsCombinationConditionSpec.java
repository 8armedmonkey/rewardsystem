package sys.reward;

import org.junit.Test;
import sys.event.EventCounts;

public class TestTriggerEventsCombinationConditionSpec {

    @Test(expected = IllegalArgumentException.class)
    public void constructor_emptyEventCounts_throwIllegalArgumentException() {
        new TriggerEventsCombinationConditionSpec.Builder()
                .eventCounts(new EventCounts())
                .build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void setEventCounts_emptyExpectedEventCounts_throwIllegalArgumentException() {
        EventCounts eventCounts = new EventCounts();
        eventCounts.set(SampleEvent1.class, 0, 1);
        eventCounts.set(SampleEvent2.class, 0, 1);
        eventCounts.set(SampleEvent3.class, 0, 1);

        TriggerEventsCombinationConditionSpec spec =
                new TriggerEventsCombinationConditionSpec.Builder()
                        .eventCounts(eventCounts)
                        .build();

        spec.setEventCounts(new EventCounts());
    }

}
