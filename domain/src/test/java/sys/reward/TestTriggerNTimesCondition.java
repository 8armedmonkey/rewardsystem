package sys.reward;

import org.junit.Before;
import org.junit.Test;

import static java.lang.System.currentTimeMillis;
import static org.hamcrest.Matchers.closeTo;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

public class TestTriggerNTimesCondition {

    private TriggerNTimesCondition condition;

    @Before
    public void setUp() {
        condition = new TriggerNTimesCondition(
                new TriggerNTimesConditionSpec.Builder()
                        .eventOccurrenceCount(0)
                        .expectedEventOccurrenceCount(3)
                        .build(),
                new RewardSpec(mock(Reward.class), false));
    }

    @Test
    public void process_oneEventClass_countIncremented() {
        condition.registerObservedEvent(SampleEvent1.class);

        condition.process(new SampleEvent1());
        condition.process(new SampleEvent1());

        assertEquals(2, condition.getEventOccurrenceCount());
    }

    @Test
    public void process_multipleEventClasses_countIncremented() {
        condition.registerObservedEvent(SampleEvent1.class);
        condition.registerObservedEvent(SampleEvent2.class);

        condition.process(new SampleEvent1());
        condition.process(new SampleEvent2());
        condition.process(new SampleEvent1());

        assertEquals(3, condition.getEventOccurrenceCount());
    }

    @Test
    public void process_notObservedEventClass_countIsNotIncremented() {
        condition.registerObservedEvent(SampleEvent1.class);

        condition.process(new SampleEvent2());
        condition.process(new SampleEvent2());
        condition.process(new SampleEvent2());

        assertEquals(0, condition.getEventOccurrenceCount());
    }

    @Test
    public void process_expectedEventOccurrenceCountReached_returnTrue() {
        condition.registerObservedEvent(SampleEvent1.class);

        assertFalse(condition.process(new SampleEvent1()));
        assertFalse(condition.process(new SampleEvent1()));
        assertTrue(condition.process(new SampleEvent1()));
    }

    @Test
    public void hasBeenFulfilled_eventOccurrenceCountReached_returnTrue() {
        condition.registerObservedEvent(SampleEvent1.class);

        condition.process(new SampleEvent1());
        condition.process(new SampleEvent1());
        condition.process(new SampleEvent1());

        assertTrue(condition.hasBeenFulfilled());
    }

    @Test
    public void hasBeenFulfilled_eventOccurrenceCountNotReached_returnFalse() {
        condition.registerObservedEvent(SampleEvent1.class);

        condition.process(new SampleEvent1());

        assertFalse(condition.hasBeenFulfilled());
    }

    @Test
    public void getConditionFulfilledTimeMillis_hasBeenFulfilled_returnTimeWhenTheConditionIsFulfilled() throws Exception {
        condition.registerObservedEvent(SampleEvent1.class);

        condition.process(new SampleEvent1());
        condition.process(new SampleEvent1());
        condition.process(new SampleEvent1());

        assertThat((double) condition.getConditionFulfilledTimeMillis(), closeTo(currentTimeMillis(), 100.0));
    }

    @Test(expected = ConditionNotFulfilledException.class)
    public void getConditionFulfilledTimeMillis_hasNotBeenFulfilled_throwConditionNotFulfilledException() throws Exception {
        condition.getConditionFulfilledTimeMillis();
    }

    @Test
    public void reset_eventOccurrenceCountResetToZero() {
        condition.registerObservedEvent(SampleEvent1.class);

        condition.process(new SampleEvent1());
        condition.process(new SampleEvent1());
        condition.process(new SampleEvent1());
        condition.reset();

        assertEquals(0, condition.getEventOccurrenceCount());
    }

}
