package sys.reward;

import org.junit.Before;
import org.junit.Test;
import sys.event.EventClasses;

import static java.lang.System.currentTimeMillis;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

public class TestTriggerEventsInSequenceCondition {

    private TriggerEventsInSequenceCondition condition;

    @Before
    public void setUp() {
        EventClasses eventClasses = new EventClasses();
        eventClasses.add(SampleEvent2.class);
        eventClasses.add(SampleEvent1.class);
        eventClasses.add(SampleEvent3.class);

        condition = new TriggerEventsInSequenceCondition(
                new TriggerEventsInSequenceConditionSpec.Builder()
                        .eventClasses(eventClasses)
                        .build(),
                new RewardSpec(mock(Reward.class), false));

        condition.registerObservedEvent(SampleEvent1.class);
        condition.registerObservedEvent(SampleEvent2.class);
        condition.registerObservedEvent(SampleEvent3.class);
    }

    @Test
    public void process_eventsInCorrectSequence_returnTrue() {
        assertFalse(condition.process(new SampleEvent2()));
        assertFalse(condition.process(new SampleEvent1()));
        assertTrue(condition.process(new SampleEvent3()));

        assertEquals(
                (double) currentTimeMillis(),
                (double) condition.getConditionFulfilledTimeMillis(),
                100
        );
    }

    @Test
    public void process_eventsNotInCorrectSequence_returnFalse() {
        assertFalse(condition.process(new SampleEvent1()));
        assertFalse(condition.process(new SampleEvent2()));
        assertFalse(condition.process(new SampleEvent3()));
        assertFalse(condition.process(new SampleEvent2()));
        assertFalse(condition.process(new SampleEvent1()));
        assertFalse(condition.process(new SampleEvent2()));
    }

    @Test
    public void process_eventsInIncorrectThenCorrectSequence_returnTrue() {
        assertFalse(condition.process(new SampleEvent3()));
        assertFalse(condition.process(new SampleEvent1()));
        assertFalse(condition.process(new SampleEvent2()));
        assertFalse(condition.process(new SampleEvent1()));
        assertFalse(condition.process(new SampleEvent1()));
        assertFalse(condition.process(new SampleEvent2()));
        assertFalse(condition.process(new SampleEvent1()));
        assertTrue(condition.process(new SampleEvent3()));

        assertEquals(
                (double) currentTimeMillis(),
                (double) condition.getConditionFulfilledTimeMillis(),
                100
        );
    }

}
