package sys.reward;

public class SampleConditionSpecBuilder
        extends ConditionSpec.Builder<ConditionSpec, ConditionSpec.Builder> {

    @Override
    public ConditionSpec build() {
        ConditionSpec conditionSpec = new ConditionSpec();
        init(conditionSpec);

        return conditionSpec;
    }

}
