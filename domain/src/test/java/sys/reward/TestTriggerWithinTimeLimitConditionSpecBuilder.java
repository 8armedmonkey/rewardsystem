package sys.reward;

import org.junit.Before;
import org.junit.Test;
import sys.common.TimeRange;

import static org.junit.Assert.assertEquals;

public class TestTriggerWithinTimeLimitConditionSpecBuilder {

    private TriggerWithinTimeLimitConditionSpec.Builder builder;

    @Before
    public void setUp() {
        builder = new TriggerWithinTimeLimitConditionSpec.Builder();
    }

    @Test
    public void expectedEventOccurrenceCount_expectedEventOccurrenceCountSetAndBuilderReturned() {
        assertEquals(builder, builder.timeRange(new TimeRange(1000L, 3000L)));
    }

    @Test
    public void build_conditionSpecBuilt() {
        long currentTimeMillis = System.currentTimeMillis();

        TriggerWithinTimeLimitConditionSpec conditionSpec = builder
                .id("id")
                .name("name")
                .description("description")
                .hasBeenFulfilled(false)
                .fulfilledTimeMillis(currentTimeMillis)
                .timeRange(new TimeRange(currentTimeMillis - 1000, currentTimeMillis + 1000))
                .build();

        assertEquals("id", conditionSpec.getId());
        assertEquals("name", conditionSpec.getName());
        assertEquals("description", conditionSpec.getDescription());
        assertEquals(false, conditionSpec.hasBeenFulfilled());
        assertEquals(currentTimeMillis, conditionSpec.getFulfilledTimeMillis());
        assertEquals(Long.valueOf(currentTimeMillis - 1000), conditionSpec.getTimeRange().getMin());
        assertEquals(Long.valueOf(currentTimeMillis + 1000), conditionSpec.getTimeRange().getMax());
    }

}
