package sys.event;

import sys.common.ItemCollection;

@SuppressWarnings("WeakerAccess")
public class EventClasses extends ItemCollection<Class<? extends Event>> {

    public EventClasses() {
        super();
    }

    public EventClasses(Class<? extends Event>... eventClasses) {
        super(eventClasses);
    }

}
