package sys.event;

public interface CumulativeEvent extends Event {

    int getValue();

}
