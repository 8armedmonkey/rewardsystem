package sys.event;

public class BaseCumulativeEvent implements CumulativeEvent {

    private final int value;

    public BaseCumulativeEvent(int value) {
        this.value = value;
    }

    @Override
    public int getValue() {
        return value;
    }

}
