package sys.event;

import sys.common.ItemCountUp;

import java.util.*;

public class EventCounts extends ItemCountUp<Class<? extends Event>> {

    @SuppressWarnings({"Convert2Lambda", "Java8ListSort"})
    public Collection<Class<? extends Event>> getEvents() {
        Set<Class<? extends Event>> set = counts.keySet();
        List<Class<? extends Event>> list = new ArrayList<>(set);

        Collections.sort(list, new Comparator<Class<? extends Event>>() {

            @Override
            public int compare(Class<? extends Event> o1, Class<? extends Event> o2) {
                return o1.getName().compareTo(o2.getName());
            }

        });

        return list;
    }

}
