package sys.reward;

import java.io.Serializable;

@SuppressWarnings("WeakerAccess")
public class RewardSpec implements Serializable {

    private Reward reward;
    private boolean rewardHasBeenClaimed;

    public RewardSpec(Reward reward, boolean rewardHasBeenClaimed) {
        this.reward = reward;
        this.rewardHasBeenClaimed = rewardHasBeenClaimed;
    }

    public Reward getReward() {
        return reward;
    }

    public void setReward(Reward reward) {
        this.reward = reward;
    }

    public boolean hasRewardBeenClaimed() {
        return rewardHasBeenClaimed;
    }

    public void setRewardHasBeenClaimed(boolean rewardHasBeenClaimed) {
        this.rewardHasBeenClaimed = rewardHasBeenClaimed;
    }

}
