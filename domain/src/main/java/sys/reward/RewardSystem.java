package sys.reward;

import sys.common.Persistable;
import sys.common.System;

public interface RewardSystem extends System, Persistable {

    Iterable<Condition> getConditions();

    void registerCondition(Condition condition);

    void unregisterCondition(Condition condition);

    boolean contains(Condition condition);

}
