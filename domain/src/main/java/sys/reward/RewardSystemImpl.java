package sys.reward;

import sys.common.PersistenceException;
import sys.event.Event;
import sys.event.EventBroadcaster;

@SuppressWarnings("WeakerAccess")
public class RewardSystemImpl implements RewardSystem {

    private EventBroadcaster eventBroadcaster;
    private EventBroadcaster.Subscription eventSubscription;
    private ConditionRepository conditionRepository;
    private Conditions conditions;

    public RewardSystemImpl(
            ConditionRepository conditionRepository,
            EventBroadcaster eventBroadcaster) {

        this.eventBroadcaster = eventBroadcaster;
        this.conditionRepository = conditionRepository;
        this.conditions = new Conditions();
    }

    @Override
    public synchronized void start() {
        eventSubscription = eventBroadcaster.subscribe(Event.class, this::onEvent);
    }

    @Override
    public synchronized void stop() {
        if (eventSubscription != null) {
            eventSubscription.unsubscribe();
            eventSubscription = null;
        }
    }

    @Override
    public synchronized void save() throws PersistenceException {
        try {
            conditionRepository.store(conditions);
        } catch (ConditionRepositoryException e) {
            throw new PersistenceException(e);
        }
    }

    @Override
    public synchronized void restore() throws PersistenceException {
        try {
            conditions = conditionRepository.retrieve();
        } catch (ConditionRepositoryException e) {
            throw new PersistenceException(e);
        }
    }

    @Override
    public synchronized Iterable<Condition> getConditions() {
        return conditions;
    }

    @Override
    public synchronized void registerCondition(Condition condition) {
        if (!contains(condition)) {
            conditions.add(condition);
        }
    }

    @Override
    public synchronized void unregisterCondition(Condition condition) {
        conditions.remove(condition);
    }

    @Override
    public synchronized boolean contains(Condition condition) {
        return conditions.contains(condition);
    }

    void onEvent(Event event) {
        for (Condition condition : conditions) {
            if (!condition.hasBeenFulfilled() && condition.process(event)) {
                eventBroadcaster.broadcast(new ConditionFulfilled(condition));
            }
        }
    }

}
