package sys.reward;

import sys.event.Event;

import static java.lang.System.currentTimeMillis;

public class TriggerEventsCombinationCondition extends BaseCondition {

    public TriggerEventsCombinationCondition(
            TriggerEventsCombinationConditionSpec conditionSpec,
            RewardSpec rewardSpec) {

        super(conditionSpec, rewardSpec);
    }

    @Override
    public synchronized boolean process(Event event) {
        if (!hasBeenFulfilled() && getObservedEventClasses().contains(event.getClass())) {
            TriggerEventsCombinationConditionSpec spec =
                    (TriggerEventsCombinationConditionSpec) conditionSpec;

            spec.getEventCounts().increment(event.getClass());

            if (spec.getEventCounts().hasReachedExpectedCounts()) {
                setConditionFulfilled();
            }
        }
        return hasBeenFulfilled();
    }

}
