package sys.reward;

import sys.event.EventClasses;

@SuppressWarnings("WeakerAccess")
public class TriggerEventsInSequenceConditionSpec extends ConditionSpec {

    private EventClasses eventClasses;

    TriggerEventsInSequenceConditionSpec() {
    }

    public EventClasses getEventClasses() {
        return eventClasses;
    }

    public void setEventClasses(EventClasses eventClasses) {
        this.eventClasses = eventClasses;
        assertEventClassesNotEmpty();
    }

    private void assertEventClassesNotEmpty() {
        if (eventClasses.size() == 0) {
            throw new IllegalArgumentException("Event classes should not be empty.");
        }
    }

    public static class Builder extends ConditionSpec.Builder<TriggerEventsInSequenceConditionSpec, Builder> {

        private EventClasses eventClasses;

        public Builder() {
        }

        public Builder(ConditionSpec.Builder builder) {
            super(builder);
        }

        public Builder eventClasses(EventClasses eventClasses) {
            this.eventClasses = eventClasses;
            return this;
        }

        @Override
        public TriggerEventsInSequenceConditionSpec build() {
            TriggerEventsInSequenceConditionSpec spec = new TriggerEventsInSequenceConditionSpec();
            init(spec);
            spec.setEventClasses(eventClasses);

            return spec;
        }

    }

}
