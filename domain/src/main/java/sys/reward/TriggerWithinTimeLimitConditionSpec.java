package sys.reward;

import sys.common.TimeRange;

@SuppressWarnings("WeakerAccess")
public class TriggerWithinTimeLimitConditionSpec extends ConditionSpec {

    private TimeRange timeRange;

    TriggerWithinTimeLimitConditionSpec() {
    }

    public TimeRange getTimeRange() {
        return timeRange;
    }

    public static class Builder extends ConditionSpec.Builder<TriggerWithinTimeLimitConditionSpec, Builder> {

        private TimeRange timeRange;

        public Builder() {
        }

        public Builder(ConditionSpec.Builder builder) {
            super(builder);
        }

        public Builder timeRange(TimeRange timeRange) {
            this.timeRange = timeRange;
            return this;
        }

        @Override
        public TriggerWithinTimeLimitConditionSpec build() {
            TriggerWithinTimeLimitConditionSpec spec = new TriggerWithinTimeLimitConditionSpec();
            init(spec);
            spec.timeRange = timeRange;

            return spec;
        }

    }

}
