package sys.reward;

import sys.event.Event;
import sys.event.EventClasses;

import static java.lang.System.currentTimeMillis;

@SuppressWarnings("WeakerAccess")
public abstract class BaseCondition implements Condition {

    protected ConditionSpec conditionSpec;
    protected RewardSpec rewardSpec;
    protected EventClasses eventClasses;

    public BaseCondition(ConditionSpec conditionSpec, RewardSpec rewardSpec) {
        if (conditionSpec == null || rewardSpec == null) {
            throw new IllegalArgumentException();
        }

        this.conditionSpec = conditionSpec;
        this.rewardSpec = rewardSpec;
        eventClasses = new EventClasses();
    }

    @Override
    public String getIndex() {
        return id();
    }

    @Override
    public String id() {
        return conditionSpec.getId();
    }

    @Override
    public String name() {
        return conditionSpec.getName();
    }

    @Override
    public String description() {
        return conditionSpec.getDescription();
    }

    @Override
    public boolean hasBeenFulfilled() {
        return conditionSpec.hasBeenFulfilled();
    }

    @Override
    public long getConditionFulfilledTimeMillis() throws ConditionNotFulfilledException {
        if (hasBeenFulfilled()) {
            return conditionSpec.getFulfilledTimeMillis();
        } else {
            throw new ConditionNotFulfilledException();
        }
    }

    @Override
    public EventClasses getObservedEventClasses() {
        return eventClasses;
    }

    @Override
    public void registerObservedEvent(Class<? extends Event> eventClass) {
        eventClasses.addIfNotExists(eventClass);
    }

    @Override
    public void unregisterObservedEvent(Class<? extends Event> eventClass) {
        eventClasses.remove(eventClass);
    }

    @Override
    public synchronized boolean hasRewardBeenClaimed() {
        return rewardSpec.hasRewardBeenClaimed();
    }

    @Override
    public synchronized Reward claimReward() throws RewardHasBeenClaimedException, ConditionNotFulfilledException {
        if (!hasBeenFulfilled()) {
            throw new ConditionNotFulfilledException();
        } else if (hasRewardBeenClaimed()) {
            throw new RewardHasBeenClaimedException();
        } else {
            rewardSpec.setRewardHasBeenClaimed(true);
            return rewardSpec.getReward();
        }
    }

    @Override
    public Reward peekReward() {
        return rewardSpec.getReward();
    }

    protected void setConditionFulfilled() {
        conditionSpec.setHasBeenFulfilled(true);
        conditionSpec.setFulfilledTimeMillis(currentTimeMillis());
    }

}
