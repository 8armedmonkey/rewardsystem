package sys.reward;

import sys.common.TimeRange;
import sys.event.Event;

@SuppressWarnings("WeakerAccess")
public class TriggerWithinTimeLimitCondition extends BaseCondition {

    public TriggerWithinTimeLimitCondition(
            TriggerWithinTimeLimitConditionSpec conditionSpec,
            RewardSpec rewardSpec) {

        super(conditionSpec, rewardSpec);
    }

    @Override
    public synchronized boolean process(Event event) {
        long currentTimeMillis = System.currentTimeMillis();

        if (!hasBeenFulfilled() && getObservedEventClasses().contains(event.getClass())) {
            TriggerWithinTimeLimitConditionSpec spec =
                    (TriggerWithinTimeLimitConditionSpec) conditionSpec;

            if (spec.getTimeRange().contains(currentTimeMillis)) {
                setConditionFulfilled();
            }
        }
        return hasBeenFulfilled();
    }

    public TimeRange getTimeRange() {
        return ((TriggerWithinTimeLimitConditionSpec) conditionSpec).getTimeRange();
    }

}
