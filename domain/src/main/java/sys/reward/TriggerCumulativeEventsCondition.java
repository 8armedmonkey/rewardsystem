package sys.reward;

import sys.common.CountUp;
import sys.event.CumulativeEvent;
import sys.event.Event;

import static java.lang.System.currentTimeMillis;

public class TriggerCumulativeEventsCondition extends BaseCondition {

    public TriggerCumulativeEventsCondition(
            TriggerCumulativeEventsConditionSpec conditionSpec,
            RewardSpec rewardSpec) {

        super(conditionSpec, rewardSpec);
    }

    @Override
    public void registerObservedEvent(Class<? extends Event> eventClass) {
        if (CumulativeEvent.class.isAssignableFrom(eventClass)) {
            super.registerObservedEvent(eventClass);
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public synchronized boolean process(Event event) {
        if (!hasBeenFulfilled() && getObservedEventClasses().contains(event.getClass())) {
            TriggerCumulativeEventsConditionSpec spec = (TriggerCumulativeEventsConditionSpec) conditionSpec;
            CountUp countUp = spec.getCountUp();
            CumulativeEvent cumulativeEvent = (CumulativeEvent) event;

            countUp.incrementBy(cumulativeEvent.getValue());

            if (countUp.hasReachedExpected()) {
                setConditionFulfilled();
            }
        }
        return hasBeenFulfilled();
    }

}
