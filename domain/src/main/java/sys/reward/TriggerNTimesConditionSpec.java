package sys.reward;

@SuppressWarnings("WeakerAccess")
public class TriggerNTimesConditionSpec extends ConditionSpec {

    private int eventOccurrenceCount;
    private int expectedEventOccurrenceCount;

    TriggerNTimesConditionSpec() {
    }

    public synchronized void incrementEventOccurrenceCount() {
        eventOccurrenceCount = Math.min(expectedEventOccurrenceCount, eventOccurrenceCount + 1);
    }

    public synchronized void resetEventOccurenceCount() {
        eventOccurrenceCount = 0;
    }

    public synchronized int getEventOccurrenceCount() {
        return eventOccurrenceCount;
    }

    public synchronized void setEventOccurrenceCount(int eventOccurrenceCount) {
        this.eventOccurrenceCount = eventOccurrenceCount;
    }

    public synchronized int getExpectedEventOccurrenceCount() {
        return expectedEventOccurrenceCount;
    }

    public synchronized void setExpectedEventOccurrenceCount(int expectedEventOccurrenceCount) {
        this.expectedEventOccurrenceCount = expectedEventOccurrenceCount;
    }

    public static class Builder extends ConditionSpec.Builder<TriggerNTimesConditionSpec, Builder> {

        private int eventOccurrenceCount;
        private int expectedEventOccurrenceCount;

        public Builder() {
        }

        public Builder(ConditionSpec.Builder builder) {
            super(builder);
        }

        public Builder eventOccurrenceCount(int eventOccurrenceCount) {
            this.eventOccurrenceCount = eventOccurrenceCount;
            return this;
        }

        public Builder expectedEventOccurrenceCount(int expectedEventOccurrenceCount) {
            this.expectedEventOccurrenceCount = expectedEventOccurrenceCount;
            return this;
        }

        @Override
        public TriggerNTimesConditionSpec build() {
            TriggerNTimesConditionSpec spec = new TriggerNTimesConditionSpec();
            init(spec);
            spec.eventOccurrenceCount = eventOccurrenceCount;
            spec.expectedEventOccurrenceCount = expectedEventOccurrenceCount;

            return spec;
        }

    }

}
