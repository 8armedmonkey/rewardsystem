package sys.reward;

public interface ConditionRepository {

    /**
     * Retrieve all conditions.
     *
     * @return All conditions.
     */
    Conditions retrieve() throws ConditionRepositoryException;

    /**
     * Store all conditions.
     *
     * @param conditions Conditions to be stored.
     */
    void store(Conditions conditions) throws ConditionRepositoryException;

    /**
     * Update condition properties.
     *
     * @param condition Condition to be updated.
     */
    void update(Condition condition) throws ConditionRepositoryException;

}
