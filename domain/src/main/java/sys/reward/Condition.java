package sys.reward;

import sys.common.Indexable;
import sys.event.Event;
import sys.event.EventClasses;

import java.io.Serializable;

public interface Condition extends Indexable<String>, Serializable {

    /**
     * Get the ID of the condition.
     *
     * @return The ID of the condition.
     */
    String id();

    /**
     * Get the name of the condition.
     *
     * @return The name of the condition.
     */
    String name();

    /**
     * Get the description of the condition.
     *
     * @return The description of the condition.
     */
    String description();

    /**
     * Process the event and check if the condition is fulfilled.
     *
     * @param event Event to be processed.
     * @return True if the condition has been fulfilled, false otherwise.
     */
    boolean process(Event event);

    /**
     * Check if the condition has been fulfilled.
     *
     * @return True if the condition has been fulfilled, false otherwise.
     */
    boolean hasBeenFulfilled();

    /**
     * Get the timestamp when the condition is fulfilled.
     *
     * @return Timestamp when the condition is fulfilled (in milliseconds).
     */
    long getConditionFulfilledTimeMillis() throws ConditionNotFulfilledException;

    /**
     * Get the list of event classes that this condition is observing / interested.
     *
     * @return List of observed event classes.
     */
    EventClasses getObservedEventClasses();

    /**
     * Register event class to be observed by this condition.
     *
     * @param eventClass Event class that is to be observed.
     */
    void registerObservedEvent(Class<? extends Event> eventClass);

    /**
     * Unregister event class from being observed by this condition.
     *
     * @param eventClass Event class that is to be removed from being observed.
     */
    void unregisterObservedEvent(Class<? extends Event> eventClass);

    /**
     * Check if the reward has been claimed.
     *
     * @return True if reward has been claimed, false otherwise.
     */
    boolean hasRewardBeenClaimed();

    /**
     * Claim the reward for fulfilling the condition.
     * If the condition has not been fulfilled, it will throw error.
     * <p>
     * After being claimed, the condition will throw {@link RewardHasBeenClaimedException}
     * if there is an attempt to claim the reward again.
     *
     * @return Reward if the condition has been fulfilled.
     */
    Reward claimReward() throws RewardHasBeenClaimedException, ConditionNotFulfilledException;

    /**
     * Peek the reward for fulfilling the condition.
     * It will not mark the reward as has been claimed.
     *
     * @return Reward if the condition has been fulfilled.
     */
    Reward peekReward();

}
