package sys.reward;

import sys.event.EventCounts;

public class TriggerEventsCombinationConditionSpec extends ConditionSpec {

    private EventCounts eventCounts;

    TriggerEventsCombinationConditionSpec() {
    }

    public EventCounts getEventCounts() {
        return eventCounts;
    }

    public void setEventCounts(EventCounts expectedEventCounts) {
        this.eventCounts = expectedEventCounts;
        assertEventCountsNotEmpty();
    }

    private void assertEventCountsNotEmpty() {
        if (eventCounts.size() == 0) {
            throw new IllegalArgumentException("Event counts should not be empty.");
        }
    }

    public static class Builder
            extends ConditionSpec.Builder<TriggerEventsCombinationConditionSpec, TriggerEventsCombinationConditionSpec.Builder> {

        private EventCounts expectedEventCounts;

        public Builder() {
        }

        public Builder(ConditionSpec.Builder builder) {
            super(builder);
        }

        public TriggerEventsCombinationConditionSpec.Builder eventCounts(EventCounts eventCounts) {
            this.expectedEventCounts = eventCounts;
            return this;
        }

        @Override
        public TriggerEventsCombinationConditionSpec build() {
            TriggerEventsCombinationConditionSpec spec = new TriggerEventsCombinationConditionSpec();
            init(spec);
            spec.setEventCounts(expectedEventCounts);

            return spec;
        }

    }

}
