package sys.reward;

import java.io.Serializable;

@SuppressWarnings("WeakerAccess")
public class ConditionSpec implements Serializable {

    String id;
    String name;
    String description;
    boolean hasBeenFulfilled;
    long fulfilledTimeMillis;

    ConditionSpec() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean hasBeenFulfilled() {
        return hasBeenFulfilled;
    }

    public void setHasBeenFulfilled(boolean hasBeenFulfilled) {
        this.hasBeenFulfilled = hasBeenFulfilled;
    }

    public long getFulfilledTimeMillis() {
        return fulfilledTimeMillis;
    }

    public void setFulfilledTimeMillis(long fulfilledTimeMillis) {
        this.fulfilledTimeMillis = fulfilledTimeMillis;
    }

    @SuppressWarnings("unchecked")
    public abstract static class Builder<C extends ConditionSpec, B extends Builder> {

        protected String id;
        protected String name;
        protected String description;
        protected boolean hasBeenFulfilled;
        protected long fulfilledTimeMillis;

        public Builder() {
        }

        public Builder(ConditionSpec.Builder builder) {
            id(builder.id)
                    .name(builder.name)
                    .description(builder.description)
                    .hasBeenFulfilled(builder.hasBeenFulfilled)
                    .fulfilledTimeMillis(builder.fulfilledTimeMillis);
        }

        public B id(String id) {
            this.id = id;
            return (B) this;
        }

        public B name(String name) {
            this.name = name;
            return (B) this;
        }

        public B description(String description) {
            this.description = description;
            return (B) this;
        }

        public B hasBeenFulfilled(boolean hasBeenFulfilled) {
            this.hasBeenFulfilled = hasBeenFulfilled;
            return (B) this;
        }

        public B fulfilledTimeMillis(long fulfilledTimeMillis) {
            this.fulfilledTimeMillis = fulfilledTimeMillis;
            return (B) this;
        }

        public abstract C build();

        protected void init(C c) {
            c.id = id;
            c.name = name;
            c.description = description;
            c.hasBeenFulfilled = hasBeenFulfilled;
            c.fulfilledTimeMillis = fulfilledTimeMillis;
        }

    }

    public static class SimpleBuilder extends Builder<ConditionSpec, ConditionSpec.Builder> {

        @Override
        public ConditionSpec build() {
            ConditionSpec conditionSpec = new ConditionSpec();
            init(conditionSpec);

            return conditionSpec;
        }

    }

}
