package sys.reward;

import sys.common.CountUp;

public class TriggerCumulativeEventsConditionSpec extends ConditionSpec {

    private CountUp countUp;

    TriggerCumulativeEventsConditionSpec() {
    }

    public CountUp getCountUp() {
        return countUp;
    }

    public static class Builder
            extends ConditionSpec.Builder<TriggerCumulativeEventsConditionSpec, TriggerCumulativeEventsConditionSpec.Builder> {

        private CountUp countUp;

        public Builder() {
        }

        public Builder(ConditionSpec.Builder builder) {
            super(builder);
        }

        public Builder countUp(CountUp countUp) {
            this.countUp = countUp;
            return this;
        }

        @Override
        public TriggerCumulativeEventsConditionSpec build() {
            TriggerCumulativeEventsConditionSpec spec = new TriggerCumulativeEventsConditionSpec();
            init(spec);
            spec.countUp = countUp;

            return spec;
        }

    }

}
