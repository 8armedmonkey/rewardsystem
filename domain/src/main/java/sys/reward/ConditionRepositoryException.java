package sys.reward;

public class ConditionRepositoryException extends Exception {

    public ConditionRepositoryException(Throwable cause) {
        super(cause);
    }

}
