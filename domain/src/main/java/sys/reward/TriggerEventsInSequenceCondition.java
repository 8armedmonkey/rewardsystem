package sys.reward;

import sys.event.Event;
import sys.event.EventClasses;

import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

@SuppressWarnings("WeakerAccess")
public class TriggerEventsInSequenceCondition extends BaseCondition {

    Queue<Class<? extends Event>> sequence;

    public TriggerEventsInSequenceCondition(
            TriggerEventsInSequenceConditionSpec conditionSpec,
            RewardSpec rewardSpec) {

        super(conditionSpec, rewardSpec);
        sequence = new ArrayBlockingQueue<>(
                conditionSpec.getEventClasses().size(), true);
    }

    @SuppressWarnings("unchecked")
    @Override
    public synchronized boolean process(Event event) {
        if (!hasBeenFulfilled() && getObservedEventClasses().contains(event.getClass())) {
            if (((ArrayBlockingQueue) sequence).remainingCapacity() == 0) {
                ((ArrayBlockingQueue) sequence).poll();
            }

            ((ArrayBlockingQueue) sequence).offer(event.getClass());

            if (verifySequence()) {
                setConditionFulfilled();
            }
        }
        return hasBeenFulfilled();
    }

    @SuppressWarnings("unchecked")
    private boolean verifySequence() {
        TriggerEventsInSequenceConditionSpec spec =
                (TriggerEventsInSequenceConditionSpec) conditionSpec;

        EventClasses eventClasses = spec.getEventClasses();
        Class[] seqArray = sequence.toArray(new Class[0]);

        // Check if the lengths are the same.
        boolean same = eventClasses.size() == seqArray.length;

        // Check if the sequence elements are the same.
        for (int i = 0, n = eventClasses.size(); same && i < n; i++) {
            Class eventClass = eventClasses.get(i);
            Class seqEventClass = seqArray[i];

            if (!eventClass.equals(seqEventClass)) {
                same = false;
            }
        }
        return same;
    }

}
