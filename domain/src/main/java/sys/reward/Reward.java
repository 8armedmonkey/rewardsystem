package sys.reward;

import java.io.Serializable;

@SuppressWarnings("WeakerAccess")
public interface Reward extends Serializable {

    String id();

    String name();

    String description();

}
