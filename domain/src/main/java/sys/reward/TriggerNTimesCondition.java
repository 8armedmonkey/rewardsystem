package sys.reward;

import sys.event.Event;

import static java.lang.System.currentTimeMillis;

@SuppressWarnings("WeakerAccess")
public class TriggerNTimesCondition extends BaseCondition {

    public TriggerNTimesCondition(
            TriggerNTimesConditionSpec conditionSpec,
            RewardSpec rewardSpec) {

        super(conditionSpec, rewardSpec);
    }

    @Override
    public synchronized boolean process(Event event) {
        if (!hasBeenFulfilled() && getObservedEventClasses().contains(event.getClass())) {
            TriggerNTimesConditionSpec spec = (TriggerNTimesConditionSpec) conditionSpec;
            spec.incrementEventOccurrenceCount();

            if (spec.getEventOccurrenceCount() >= spec.getExpectedEventOccurrenceCount()) {
                setConditionFulfilled();
            }
        }
        return hasBeenFulfilled();
    }

    public int getEventOccurrenceCount() {
        return ((TriggerNTimesConditionSpec) conditionSpec).getEventOccurrenceCount();
    }

    public synchronized void reset() {
        ((TriggerNTimesConditionSpec) conditionSpec).resetEventOccurenceCount();
    }

}
