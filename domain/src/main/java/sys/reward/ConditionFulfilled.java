package sys.reward;

import sys.event.Event;

@SuppressWarnings("WeakerAccess")
public class ConditionFulfilled implements Event {

    private final Condition condition;

    public ConditionFulfilled(Condition condition) {
        this.condition = condition;
    }

    public Condition getCondition() {
        return condition;
    }

}
