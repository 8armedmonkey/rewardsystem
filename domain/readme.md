## Reward System

Reward System will listen to event broadcast. The event is then passed to the registered conditions to be processed.
Conditions that have not been fulfilled will process the event if they are interested in the event. 

When a condition is fulfilled, Reward System will broadcast `ConditionFulfilled` event.

### Registering Condition

Multiple conditions could be registered to the Reward System:

`RewardSystem::registerCondition(Condition)`

### Un-registering Condition

To unregister a specific condition from the Reward System:

`RewardSystem::unregisterCondition(Condition)`

### Saving State

Saving state is intended as a blocking operation, but it depends on the `ConditionRepository` implementation (could be implemented as blocking or non-blocking).

`RewardSystem::save()`

### Restoring State

Restoring state is intended as a blocking operation, but it depends on the `ConditionRepository` implementation (could be implemented as blocking or non-blocking).

`RewardSystem::restore()`

## Condition

Each condition contains a reward. When the condition is fulfilled, its reward becomes eligible for collection.
The condition's reward could only be claimed once. After the reward has been claimed, any attempt to claim the reward again
will result in `RewardHasBeenClaimedException` being thrown.

### Condition Types
* `TriggerNTimesCondition`: fulfilled when the observed events are triggered several (N) times. 
* `TriggerEventsInSequenceCondition`: fulfilled when specific events are triggered in sequence.
* `TriggerEventsCombinationCondition`: fulfilled when specific events are triggered in or out of sequence.
* `TriggerCumulativeEventsCondition`: fulfilled when cumulative events are triggered and the cumulative value reaches the limit required by the condition.
* `TriggerWithinTimeLimitCondition`: fulfilled when a specific event is triggered within a certain time span.