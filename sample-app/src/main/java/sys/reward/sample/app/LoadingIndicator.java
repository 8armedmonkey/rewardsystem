package sys.reward.sample.app;

import java.util.concurrent.atomic.AtomicBoolean;

import static java.lang.String.format;
import static java.lang.System.out;
import static sys.util.ThreadUtils.interrupt;

public class LoadingIndicator {

    private static final String LOADING_STATES = "|/–\\";
    private AtomicBoolean running;
    private Thread t;

    public LoadingIndicator() {
        running = new AtomicBoolean(false);
    }

    public void start(String message, Position position) {
        if (!running.getAndSet(true)) {
            interrupt(t);

            t = new Thread(() -> startInternal(message, position));
            t.start();
        }
    }

    public void stop(String message) {
        if (running.getAndSet(false)) {
            interrupt(t);

            out.println();
            out.println(message);
        }
    }

    private void startInternal(String message, Position position) {
        for (int i = 0, n = LOADING_STATES.length(); !Thread.interrupted() && running.get(); i = (i + 1) % n) {
            if (position == Position.START) {
                out.print(format("\r%s %s", LOADING_STATES.charAt(i), message));

            } else if (position == Position.END) {
                out.print(format("\r%s %s", message, LOADING_STATES.charAt(i)));
            }

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                stop("");
            }
        }
    }

    enum Position {
        START, END
    }

}
