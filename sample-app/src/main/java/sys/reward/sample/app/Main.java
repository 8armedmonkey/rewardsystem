package sys.reward.sample.app;

import io.reactivex.Observable;
import sys.cmd.Command;
import sys.cmd.Commands;
import sys.cmd.SimpleCommand;
import sys.common.BooleanComparator;
import sys.common.PersistenceException;
import sys.common.SortOrder;
import sys.event.EventBroadcaster;
import sys.event.EventBroadcasterFactoryImpl;
import sys.reward.*;
import sys.reward.sample.event.*;
import sys.reward.sample.reward.*;
import sys.util.SerializationUtils;

import java.io.File;
import java.util.Iterator;
import java.util.Scanner;

import static java.lang.String.format;
import static java.lang.System.out;
import static sys.reward.sample.util.ColorUtils.AnsiColor.*;
import static sys.reward.sample.util.ColorUtils.colorize;

public class Main {

    private static final String CONDITIONS_REPOSITORY = "conditions.repo";

    private EventBroadcaster eventBroadcaster;
    private RewardSystem rewardSystem;
    private Scanner scanner;
    private Commands commands;
    private LoadingIndicator loadingIndicator;
    private volatile boolean done;

    public static void main(String[] args) {
        Main main = new Main();
        main.run();
    }

    private Main() {
        eventBroadcaster = new EventBroadcasterFactoryImpl().createEventBroadcaster();
        eventBroadcaster.subscribe(ConditionFulfilled.class, this::handleConditionFulfilled);

        ConditionRepository conditionRepository = new JsonConditionRepository(new File(CONDITIONS_REPOSITORY));
        rewardSystem = new RewardSystemImpl(conditionRepository, eventBroadcaster);

        scanner = new Scanner(System.in);
        scanner.useDelimiter("[^\\s]+\n");

        commands = new Commands();
        commands.add(new SimpleCommand("help", "to show this instruction", this::showInstructions));
        commands.add(new SimpleCommand("list", "to list the conditions", this::showConditions));
        commands.add(new SimpleCommand("save", "to save the state", this::saveState));
        commands.add(new SimpleCommand("reset", "to reset the state (all saved data will be lost)", this::resetState));
        commands.add(new SimpleCommand("foo", "to trigger the 'foo' event", () -> eventBroadcaster.broadcast(new AnswerWithFoo())));
        commands.add(new SimpleCommand("bar", "to trigger the 'bar' event", () -> eventBroadcaster.broadcast(new AnswerWithBar())));
        commands.add(new SimpleCommand("one", "to trigger the 'one' event", () -> eventBroadcaster.broadcast(new AnswerWithOne())));
        commands.add(new SimpleCommand("two", "to trigger the 'two' event", () -> eventBroadcaster.broadcast(new AnswerWithTwo())));
        commands.add(new SimpleCommand("three", "to trigger the 'three' event", () -> eventBroadcaster.broadcast(new AnswerWithThree())));
        commands.add(new SimpleCommand("[positive number]", "to trigger cumulative event", () -> {}));
        commands.add(new SimpleCommand("exit", "to exit the app", () -> done = true));

        loadingIndicator = new LoadingIndicator();

        done = false;
    }

    private void init() {
        restoreState();
        rewardSystem.start();
    }

    private Conditions createInitialConditions() {
        Conditions conditions = new Conditions();
        int conditionId = 0, rewardId = 0;

        conditions.add(new SampleTriggerNTimesConditionFactory().createCondition(
                String.valueOf(++conditionId), String.valueOf(++rewardId)));

        conditions.add(new SampleTriggerEventsInSequenceConditionFactory().createCondition(
                String.valueOf(++conditionId), String.valueOf(++rewardId)));

        conditions.add(new SampleTriggerWithinTimeLimitConditionFactory().createCondition(
                String.valueOf(++conditionId), String.valueOf(++rewardId)));

        conditions.add(new SampleTriggerEventsCombinationConditionFactory().createCondition(
                String.valueOf(++conditionId), String.valueOf(++rewardId)));

        conditions.add(new SampleTriggerCumulativeEventsConditionFactory().createCondition(
                String.valueOf(++conditionId), String.valueOf(++rewardId)));

        return conditions;
    }

    private void clearCurrentConditions() {
        Iterator<Condition> iterator;

        while ((iterator = rewardSystem.getConditions().iterator()).hasNext()) {
            rewardSystem.unregisterCondition(iterator.next());
        }
    }

    private void restoreState() {
        try {
            loadingIndicator.start("Restoring, please wait...", LoadingIndicator.Position.END);
            rewardSystem.restore();
            loadingIndicator.stop("Restore complete");

        } catch (PersistenceException e) {
            loadingIndicator.stop(format("Failed to restore, stack trace:\n%s", SerializationUtils.toString(e)));
            resetState();
        }
    }

    private void saveState() {
        try {
            loadingIndicator.start("Saving...", LoadingIndicator.Position.END);
            rewardSystem.save();
            loadingIndicator.stop("Saved");

        } catch (PersistenceException e) {
            loadingIndicator.stop(format("Failed to save, stack trace:\n%s", SerializationUtils.toString(e)));
        }
    }

    private void resetState() {
        out.print("Resetting...");

        clearCurrentConditions();

        Conditions conditions = createInitialConditions();

        for (Condition condition : conditions) {
            rewardSystem.registerCondition(condition);
        }

        out.println("Reset complete");

        saveState();
    }

    private String prompt() {
        out.print("\n> ");
        return scanner.nextLine().trim();
    }

    private void showInstructions() {
        StringBuilder sb = new StringBuilder()
                .append("\n\n")
                .append("INSTRUCTION\n")
                .append("-----------\n")
                .append("Type the following:\n\n");

        for (Command command : commands) {
            sb.append(format("\t%-20s %-50s\n", command.getName(), command.getDescription()));
        }

        sb.append("\n");

        out.println(sb.toString());
    }

    private void showConditions() {
        StringBuilder sb = new StringBuilder()
                .append("\n\n")
                .append("CONDITIONS\n")
                .append("----------\n");

        BooleanComparator bc = new BooleanComparator(SortOrder.ASC);

        Observable.fromIterable(rewardSystem.getConditions())
                .sorted((o1, o2) -> bc.compare(o1.hasBeenFulfilled(), o2.hasBeenFulfilled()))
                .subscribe(condition -> appendCondition(sb, condition));

        sb.append("\n");

        out.println(sb.toString());
    }

    private void appendCondition(StringBuilder sb, Condition condition) {
        String title = format("%s %s",
                condition.hasBeenFulfilled() ? "[√]" : "[ ]",
                condition.name());

        sb.append(colorize(title, condition.hasBeenFulfilled() ? GREEN : RED))
                .append("\n")
                .append(condition.description())
                .append("\n\n");
    }

    private void run() {
        init();
        showInstructions();

        while (!done) {
            String input = prompt();
            out.println();

            if (commands.containsKey(input)) {
                commands.getByIndex(input).execute();
            } else {
                tryHandleNumericInput(input);
            }
        }

        exit();
    }

    private void tryHandleNumericInput(String input) {
        try {
            int number = Integer.parseInt(input);

            if (number > 0) {
                eventBroadcaster.broadcast(new AnswerWithNumber(number));
            }
        } catch (NumberFormatException e) {
            // Ignore error.
        }
    }

    private void exit() {
        scanner.close();
        rewardSystem.stop();
        System.exit(0);
    }

    private void handleConditionFulfilled(ConditionFulfilled event) {
        Condition condition = event.getCondition();
        Reward reward = condition.peekReward();

        String message = format("Fulfilled condition: %s. Reward: %s",
                condition.name(), reward.name());

        out.println(colorize(message, YELLOW));
    }

}
