package sys.reward.sample.reward;

import sys.reward.Condition;
import sys.reward.ConditionRepository;
import sys.reward.ConditionRepositoryException;
import sys.reward.Conditions;

import java.io.*;

public class SampleConditionRepository implements ConditionRepository {

    private final File file;

    public SampleConditionRepository(File file) {
        this.file = file;
    }

    @Override
    public Conditions retrieve() throws ConditionRepositoryException {
        try (ObjectInputStream is = new ObjectInputStream(new FileInputStream(file))) {

            sleep();
            return (Conditions) is.readObject();

        } catch (IOException | ClassNotFoundException e) {
            throw new ConditionRepositoryException(e);
        }
    }

    @Override
    public void store(Conditions conditions) throws ConditionRepositoryException {
        try (ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(file))) {

            sleep();
            os.writeObject(conditions);

        } catch (IOException e) {
            throw new ConditionRepositoryException(e);
        }
    }

    @Override
    public void update(Condition condition) throws ConditionRepositoryException {
        Conditions conditions = retrieve();
        conditions.replace(condition);
        store(conditions);
    }

    private void sleep() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            // Ignore error.
        }
    }

}
