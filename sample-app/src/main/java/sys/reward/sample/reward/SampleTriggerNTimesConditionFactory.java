package sys.reward.sample.reward;

import sys.reward.RewardSpec;
import sys.reward.TriggerNTimesCondition;
import sys.reward.TriggerNTimesConditionSpec;
import sys.reward.sample.event.AnswerWithFoo;

public class SampleTriggerNTimesConditionFactory extends AbstractSampleConditionFactory<TriggerNTimesCondition> {

    @Override
    public TriggerNTimesCondition createCondition(String conditionId, String rewardId) {
        TriggerNTimesCondition condition = new TriggerNTimesCondition(
                new TriggerNTimesConditionSpec.Builder()
                        .id(conditionId)
                        .name("3 Foo's")
                        .description("Get the reward by answering 'foo' 3 times.")
                        .eventOccurrenceCount(0)
                        .expectedEventOccurrenceCount(3)
                        .build(),

                new RewardSpec(new SampleReward(
                        rewardId, "Foo-Foo-Foo", "This reward is obtained after answering 'foo' 3 times."
                ), false)
        );

        condition.registerObservedEvent(AnswerWithFoo.class);

        return condition;
    }

}
