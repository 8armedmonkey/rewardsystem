package sys.reward.sample.reward;

import sys.reward.Reward;

public class SampleReward implements Reward {

    private String id;
    private String name;
    private String description;

    public SampleReward(String id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    @Override
    public String id() {
        return id;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public String description() {
        return description;
    }

}
