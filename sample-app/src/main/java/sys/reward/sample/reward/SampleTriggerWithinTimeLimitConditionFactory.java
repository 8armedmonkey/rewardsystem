package sys.reward.sample.reward;

import sys.common.TimeRange;
import sys.reward.RewardSpec;
import sys.reward.TriggerWithinTimeLimitCondition;
import sys.reward.TriggerWithinTimeLimitConditionSpec;
import sys.reward.sample.event.AnswerWithFoo;

import static java.lang.System.currentTimeMillis;

public class SampleTriggerWithinTimeLimitConditionFactory
        extends AbstractSampleConditionFactory<TriggerWithinTimeLimitCondition> {

    @Override
    public TriggerWithinTimeLimitCondition createCondition(String conditionId, String rewardId) {
        TriggerWithinTimeLimitCondition condition = new TriggerWithinTimeLimitCondition(
                new TriggerWithinTimeLimitConditionSpec.Builder()
                        .id(conditionId)
                        .name("Foo 10 Seconds Startup")
                        .description("Get the reward by answering 'foo' within 10 secs after the app is started")
                        .timeRange(new TimeRange(currentTimeMillis(), currentTimeMillis() + 10000))
                        .build(),

                new RewardSpec(new SampleReward(
                        rewardId, "Foo-10-Secs", "This reward is obtained by answering 'foo' within 10 secs after the app is started."
                ), false)
        );

        condition.registerObservedEvent(AnswerWithFoo.class);

        return condition;
    }

}
