package sys.reward.sample.reward;

import sys.event.EventCounts;
import sys.reward.RewardSpec;
import sys.reward.TriggerEventsCombinationCondition;
import sys.reward.TriggerEventsCombinationConditionSpec;
import sys.reward.sample.event.AnswerWithOne;
import sys.reward.sample.event.AnswerWithThree;
import sys.reward.sample.event.AnswerWithTwo;

public class SampleTriggerEventsCombinationConditionFactory
        extends AbstractSampleConditionFactory<TriggerEventsCombinationCondition> {

    @Override
    public TriggerEventsCombinationCondition createCondition(String conditionId, String rewardId) {
        EventCounts eventCounts = new EventCounts();
        eventCounts.set(AnswerWithOne.class, 0, 1);
        eventCounts.set(AnswerWithTwo.class, 0, 2);
        eventCounts.set(AnswerWithThree.class, 0, 3);

        TriggerEventsCombinationCondition condition = new TriggerEventsCombinationCondition(
                new TriggerEventsCombinationConditionSpec.Builder()
                        .id(conditionId)
                        .name("1 One, 2 Two's, 3 Three's")
                        .description("Get the reward by answering 'one' once, 'two' twice, 'three' thrice.")
                        .eventCounts(eventCounts)
                        .build(),

                new RewardSpec(new SampleReward(
                        rewardId, "1-2-3", "This reward is obtained when \"1 One, 2 Two's, 3 Three's\" condition is fulfilled."
                ), false)
        );

        condition.registerObservedEvent(AnswerWithOne.class);
        condition.registerObservedEvent(AnswerWithTwo.class);
        condition.registerObservedEvent(AnswerWithThree.class);

        return condition;
    }

}
