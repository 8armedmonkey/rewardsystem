package sys.reward.sample.reward;

import sys.reward.Condition;

public abstract class AbstractSampleConditionFactory<C extends Condition> {

    public abstract C createCondition(String conditionId, String rewardId);

}
