package sys.reward.sample.reward;

import sys.common.CountUp;
import sys.reward.RewardSpec;
import sys.reward.TriggerCumulativeEventsCondition;
import sys.reward.TriggerCumulativeEventsConditionSpec;
import sys.reward.sample.event.AnswerWithNumber;

public class SampleTriggerCumulativeEventsConditionFactory
        extends AbstractSampleConditionFactory<TriggerCumulativeEventsCondition> {

    @Override
    public TriggerCumulativeEventsCondition createCondition(String conditionId, String rewardId) {
        TriggerCumulativeEventsCondition condition = new TriggerCumulativeEventsCondition(
                new TriggerCumulativeEventsConditionSpec.Builder()
                        .id(conditionId)
                        .name("100 Points")
                        .description("Get the reward by answering numbers until the sum is 100.")
                        .countUp(new CountUp(0, 100))
                        .build(),

                new RewardSpec(new SampleReward(
                        rewardId, "100-Pts", "This reward is obtained when \"100 Points\" condition is fulfilled."
                ), false)
        );

        condition.registerObservedEvent(AnswerWithNumber.class);

        return condition;
    }

}
