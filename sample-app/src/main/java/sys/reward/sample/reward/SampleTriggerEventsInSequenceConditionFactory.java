package sys.reward.sample.reward;

import sys.event.EventClasses;
import sys.reward.RewardSpec;
import sys.reward.TriggerEventsInSequenceCondition;
import sys.reward.TriggerEventsInSequenceConditionSpec;
import sys.reward.sample.event.*;

public class SampleTriggerEventsInSequenceConditionFactory
        extends AbstractSampleConditionFactory<TriggerEventsInSequenceCondition> {

    @SuppressWarnings("unchecked")
    @Override
    public TriggerEventsInSequenceCondition createCondition(String conditionId, String rewardId) {
        TriggerEventsInSequenceCondition condition = new TriggerEventsInSequenceCondition(
                new TriggerEventsInSequenceConditionSpec.Builder()
                        .id(conditionId)
                        .name("Foo & Bar")
                        .description("Get the reward by answering 'foo' then 'bar'.")
                        .eventClasses(new EventClasses(AnswerWithFoo.class, AnswerWithBar.class))
                        .build(),

                new RewardSpec(new SampleReward(
                        rewardId, "Foo-Bar", "This reward is obtained by answering 'foo' then 'bar'."
                ), false)
        );

        condition.registerObservedEvent(AnswerWithFoo.class);
        condition.registerObservedEvent(AnswerWithBar.class);
        condition.registerObservedEvent(AnswerWithNumber.class);
        condition.registerObservedEvent(AnswerWithOne.class);
        condition.registerObservedEvent(AnswerWithTwo.class);
        condition.registerObservedEvent(AnswerWithThree.class);

        return condition;
    }

}
