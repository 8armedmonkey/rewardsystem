package sys.reward.sample.reward;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import sys.common.CountUp;
import sys.common.TimeRange;
import sys.event.EventClasses;
import sys.event.EventCounts;
import sys.reward.*;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Queue;

public class JsonConditionRepository implements ConditionRepository {

    private final File file;
    private final Gson gson;

    public JsonConditionRepository(File file) {
        this.file = file;
        this.gson = new GsonBuilder()
                .registerTypeHierarchyAdapter(ConditionSpec.class, new ConditionSpecTypeAdapter())
                .registerTypeHierarchyAdapter(Condition.class, new ConditionTypeAdapter(new SampleRewardTypeAdapter()))
                .registerTypeAdapter(CountUp.class, new CountUpTypeAdapter())
                .registerTypeAdapter(EventClasses.class, new EventClassesTypeAdapter())
                .registerTypeAdapter(EventCounts.class, new EventCountsTypeAdapter())
                .registerTypeAdapter(TimeRange.class, new TimeRangeTypeAdapter())
                .registerTypeHierarchyAdapter(Queue.class, new QueueSequenceTypeAdapter())
                .create();
    }

    @Override
    public Conditions retrieve() throws ConditionRepositoryException {
        try (JsonReader reader = new JsonReader(new FileReader(file))) {

            sleep();
            return gson.fromJson(reader, Conditions.class);

        } catch (IOException e) {
            throw new ConditionRepositoryException(e);
        }
    }

    @Override
    public void store(Conditions conditions) throws ConditionRepositoryException {
        try (FileWriter fileWriter = new FileWriter(file)) {

            String json = gson.toJson(conditions);
            fileWriter.write(json);

            sleep();

        } catch (IOException e) {
            throw new ConditionRepositoryException(e);
        }
    }

    @Override
    public void update(Condition condition) throws ConditionRepositoryException {
        Conditions conditions = retrieve();
        conditions.replace(condition);
        store(conditions);
    }

    private void sleep() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            // Ignore error.
        }
    }

}
