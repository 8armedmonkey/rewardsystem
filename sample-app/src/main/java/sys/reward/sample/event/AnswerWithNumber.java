package sys.reward.sample.event;

import sys.event.BaseCumulativeEvent;

public class AnswerWithNumber extends BaseCumulativeEvent {

    public AnswerWithNumber(int value) {
        super(value);
    }

}
