package sys.cmd;

public class SimpleCommand implements Command {

    private final String name;
    private final String description;
    private final Runnable handler;

    public SimpleCommand(String name, String description, Runnable handler) {
        this.name = name;
        this.description = description;
        this.handler = handler;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void execute() {
        handler.run();
    }

    @Override
    public String getIndex() {
        return name;
    }
}
