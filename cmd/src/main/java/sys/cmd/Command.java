package sys.cmd;

import sys.common.Indexable;

import java.io.Serializable;

public interface Command extends Indexable<String>, Serializable {

    String getName();

    String getDescription();

    void execute();

}
