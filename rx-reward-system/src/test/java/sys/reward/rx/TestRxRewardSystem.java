package sys.reward.rx;

import io.reactivex.observers.TestObserver;
import org.junit.Before;
import org.junit.Test;
import sys.reward.Condition;
import sys.reward.RewardSystem;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class TestRxRewardSystem {

    private RewardSystem rewardSystem;
    private RxRewardSystem rxRewardSystem;

    @Before
    public void setUp() {
        rewardSystem = mock(RewardSystem.class);
        rxRewardSystem = new RxRewardSystem(rewardSystem);
    }

    @Test
    public void start_rewardSystemStarted() {
        TestObserver observer = TestObserver.create();
        rxRewardSystem.start().subscribe(observer);
        observer.awaitTerminalEvent();

        verify(rewardSystem).start();
    }

    @Test
    public void stop_rewardSystemStopped() {
        TestObserver observer = TestObserver.create();
        rxRewardSystem.stop().subscribe(observer);
        observer.awaitTerminalEvent();

        verify(rewardSystem).stop();
    }

    @Test
    public void save_stateSaved() throws Exception {
        TestObserver observer = TestObserver.create();
        rxRewardSystem.save().subscribe(observer);
        observer.awaitTerminalEvent();

        verify(rewardSystem).save();
    }

    @Test
    public void restore_stateRestored() throws Exception {
        TestObserver observer = TestObserver.create();
        rxRewardSystem.restore().subscribe(observer);
        observer.awaitTerminalEvent();

        verify(rewardSystem).restore();
    }

    @Test
    public void getConditions_conditionsReturned() {
        TestObserver observer = TestObserver.create();
        rxRewardSystem.getConditions().subscribe(observer);
        observer.awaitTerminalEvent();

        verify(rewardSystem).getConditions();
    }

    @Test
    public void registerCondition_condition_conditionRegistered() {
        TestObserver observer = TestObserver.create();
        Condition condition = mock(Condition.class);

        rxRewardSystem.registerCondition(condition).subscribe(observer);
        observer.awaitTerminalEvent();

        verify(rewardSystem).registerCondition(eq(condition));
    }

    @Test
    public void unregisterCondition_condition_conditionUnregistered() {
        TestObserver observer = TestObserver.create();
        Condition condition = mock(Condition.class);

        rxRewardSystem.unregisterCondition(condition).subscribe(observer);
        observer.awaitTerminalEvent();

        verify(rewardSystem).unregisterCondition(eq(condition));
    }

    @Test
    public void contains_conditionQueried() {
        TestObserver<Boolean> observer = TestObserver.create();
        Condition condition = mock(Condition.class);

        rxRewardSystem.contains(condition).subscribe(observer);
        observer.awaitTerminalEvent();

        verify(rewardSystem).contains(eq(condition));
    }

}
