package sys.reward.rx;

import io.reactivex.Completable;
import io.reactivex.Observable;
import sys.common.PersistenceException;
import sys.reward.Condition;
import sys.reward.RewardSystem;

public class RxRewardSystem {

    private RewardSystem rewardSystem;

    public RxRewardSystem(RewardSystem rewardSystem) {
        this.rewardSystem = rewardSystem;
    }

    public Completable start() {
        return Completable.create(subscriber -> {

            rewardSystem.start();
            subscriber.onComplete();

        });
    }

    public Completable stop() {
        return Completable.create(subscriber -> {

            rewardSystem.stop();
            subscriber.onComplete();

        });
    }

    public Completable save() {
        return Completable.create(subscriber -> {

            try {
                rewardSystem.save();
            } catch (PersistenceException e) {
                subscriber.onError(e);
            }

            subscriber.onComplete();

        });
    }

    public Completable restore() {
        return Completable.create(subscriber -> {

            try {
                rewardSystem.restore();
            } catch (PersistenceException e) {
                subscriber.onError(e);
            }

            subscriber.onComplete();

        });
    }

    public Observable<Iterable<Condition>> getConditions() {
        return Observable.create(subscriber -> {

            subscriber.onNext(rewardSystem.getConditions());
            subscriber.onComplete();

        });
    }

    public Completable registerCondition(Condition condition) {
        return Completable.create(subscriber -> {

            rewardSystem.registerCondition(condition);
            subscriber.onComplete();

        });
    }

    public Completable unregisterCondition(Condition condition) {
        return Completable.create(subscriber -> {

            rewardSystem.unregisterCondition(condition);
            subscriber.onComplete();

        });
    }

    public Observable<Boolean> contains(Condition condition) {
        return Observable.create(subscriber -> {

            subscriber.onNext(rewardSystem.contains(condition));
            subscriber.onComplete();

        });
    }

}
